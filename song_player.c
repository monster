/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 *   Play one song and be able to stop playback
 *   This file is part of monster
 *
 *   Copyright (C) 2006,2007 Nedko Arnaudov <nedko@arnaudov.name>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; version 2 of the License
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *****************************************************************************/

#include <unistd.h>
#include <pthread.h>
#include <libxml/parser.h>
#include <string.h>
#include <libgen.h>

#define DISABLE_DEBUG_OUTPUT

#include "media_source.h"
#include "player.h"
#include "media_source_dv.h"
#include "media_source_wav.h"
#include "media_source_midi.h"
#include "player_video1394.h"
#include "player_alsa_pcm.h"
#include "player_alsa_midi.h"
#include "log.h"
#include "xml.h"
#include "song_player.h"
#include "conf.h"
#include "path.h"

#define XPATH_SONG_NAME_EXPRESSION "/song/name"

#define EOD_CONTEXT_VIDEO   1
#define EOD_CONTEXT_AUDIO   2
#define EOD_CONTEXT_MIDI    4

pthread_mutex_t g_real_mask_mutex;
unsigned int g_real_mask;
eos_t g_eos;
struct media_source * g_media_source_dv_ptr;
struct media_source * g_media_source_wav_ptr;
struct media_source * g_media_source_midi_ptr;
struct player * g_player_video1394_ptr;
struct player * g_player_alsa_pcm_ptr;
struct player * g_player_alsa_midi_ptr;
struct song_descriptor * g_song_descriptor_ptr;

void
eod(unsigned int context)
{
  switch (context)
  {
  case EOD_CONTEXT_VIDEO:
    DEBUG_OUT("eod: video");
    break;
  case EOD_CONTEXT_AUDIO:
    DEBUG_OUT("eod: audio");
    break;
  case EOD_CONTEXT_MIDI:
    DEBUG_OUT("eod: MIDI");
    break;
  default:
    DEBUG_OUT("eod: %u", context);
  }

  pthread_mutex_lock(&g_real_mask_mutex);
  g_real_mask &= ~context;
  if (g_real_mask == 0)
  {
    OUTPUT_MESSAGE("====== End of song");
    g_eos();
  }
  pthread_mutex_unlock(&g_real_mask_mutex);
}

struct song_descriptor
{
  char * name_ptr;
  char * media_video_filename_ptr;
  char * media_audio_filename_ptr;
  char * media_midi_filename_ptr;
  int start_offset_video;
  int start_offset_audio;
  int start_offset_midi;
};

void
free_song_descriptor(
  struct song_descriptor * song_descriptor_ptr)
{
  free(song_descriptor_ptr->name_ptr);

  if (song_descriptor_ptr->media_video_filename_ptr != NULL)
  {
    free(song_descriptor_ptr->media_video_filename_ptr);
  }

  if (song_descriptor_ptr->media_audio_filename_ptr != NULL)
  {
    free(song_descriptor_ptr->media_audio_filename_ptr);
  }

  if (song_descriptor_ptr->media_midi_filename_ptr != NULL)
  {
    free(song_descriptor_ptr->media_midi_filename_ptr);
  }

  free(song_descriptor_ptr);
}

void
dump_song_descriptor(
  struct song_descriptor * song_descriptor_ptr)
{
  NOTICE_OUT("Song name: \"%s\"", song_descriptor_ptr->name_ptr);

  if (song_descriptor_ptr->media_video_filename_ptr != NULL)
  {
    NOTICE_OUT("Video media: \"%s\"", song_descriptor_ptr->media_video_filename_ptr);
  }

  if (song_descriptor_ptr->media_audio_filename_ptr != NULL)
  {
    NOTICE_OUT("Audio media: \"%s\"", song_descriptor_ptr->media_audio_filename_ptr);
  }

  if (song_descriptor_ptr->media_midi_filename_ptr != NULL)
  {
    NOTICE_OUT("MIDI media: \"%s\"", song_descriptor_ptr->media_midi_filename_ptr);
  }
}

int
get_song_name(
  const char * song_filename_ptr,
  char ** song_name_ptr_ptr)
{
  int ret;
  xmlDocPtr doc_ptr;

  doc_ptr = xmlParseFile(song_filename_ptr);
  if (doc_ptr == NULL)
  {
    ERROR_OUT("Failed to parse song \"%s\"", song_filename_ptr);
    ret = -1;
    goto exit;
  }

  ret = xml_get_string_dup(doc_ptr, XPATH_SONG_NAME_EXPRESSION, song_name_ptr_ptr);
  if (ret != 0)
  {
    ERROR_OUT("Cannot find song name within \"%s\"", song_filename_ptr);
    ret = -1;
    goto free_doc;
  }

  ret = 0;

free_doc:
  xmlFreeDoc(doc_ptr);

exit:
  return ret;
}

int
get_song_descriptor(
  const char * song_filename_ptr,
  struct song_descriptor ** song_descriptor_ptr_ptr)
{
  int ret, midi, audio, video;
  xmlDocPtr doc_ptr;
  struct song_descriptor * song_descriptor_ptr;
  char * media_filename_ptr;
  char * song_filename_copy_ptr;
  const char * song_dirname_ptr;
  size_t song_dirname_size;
  size_t temp_size;

  DEBUG_OUT("Parsing %s", song_filename_ptr);

  song_filename_copy_ptr = strdup(song_filename_ptr);
  if (song_filename_copy_ptr == NULL)
  {
    ERROR_OUT("strdup() failed");
    ret = -1;
    goto exit;
  }

  song_dirname_ptr = dirname(song_filename_copy_ptr);

  DEBUG_OUT("song dirname is \"%s\"", song_dirname_ptr);

  song_dirname_size = strlen(song_dirname_ptr);

  doc_ptr = xmlParseFile(song_filename_ptr);
  if (doc_ptr == NULL)
  {
    ERROR_OUT("Failed to parse song \"%s\"", song_filename_ptr);
    ret = -1;
    goto free_song_copy;
  }

  song_descriptor_ptr = (struct song_descriptor *)malloc(sizeof(struct song_descriptor));
  if (song_descriptor_ptr == NULL)
  {
    ERROR_OUT("Cannot allocate song descriptor");
    ret = -1;
    goto free_doc;
  }

  midi = xml_check_path(doc_ptr, "/song/midi");
  if (midi < 0)
  {
    ERROR_OUT("xml_check_path() failed.");
    ret = -1;
    goto free_doc;
  }

  audio = xml_check_path(doc_ptr, "/song/audio");
  if (audio < 0)
  {
    ERROR_OUT("xml_check_path() failed.");
    ret = -1;
    goto free_doc;
  }

  video = xml_check_path(doc_ptr, "/song/video");
  if (video < 0)
  {
    ERROR_OUT("xml_check_path() failed.");
    ret = -1;
    goto free_doc;
  }

  if (midi > 1)
  {
    ERROR_OUT("Only one MIDI media allowed.");
    ret = -1;
    goto free_doc;
  }

  if (audio > 1)
  {
    ERROR_OUT("Only one audio media allowed.");
    ret = -1;
    goto free_doc;
  }

  if (video > 1)
  {
    ERROR_OUT("Only one video media allowed.");
    ret = -1;
    goto free_doc;
  }

  if (midi != 0)
  {
    ret = xml_get_string_dup(doc_ptr, "/song/midi/media", &media_filename_ptr);
    if (ret != 0)
    {
      ERROR_OUT("Cannot find MIDI media filename within \"%s\"", song_filename_ptr);
      ret = -1;
      goto exit_free_song_descriptor;
    }

    NOTICE_OUT("MIDI present");

    if (media_filename_ptr[0] == '/')
    {
      song_descriptor_ptr->media_midi_filename_ptr = media_filename_ptr;
    }
    else
    {
      temp_size = strlen(media_filename_ptr);

      song_descriptor_ptr->media_midi_filename_ptr = (char *)malloc(song_dirname_size + 1 + temp_size + 1);
      if (song_descriptor_ptr->media_midi_filename_ptr == NULL)
      {
        ERROR_OUT("malloc() failed.");
        ret = -1;
        goto exit_free_song_descriptor;
      }

      memcpy(song_descriptor_ptr->media_midi_filename_ptr, song_dirname_ptr, song_dirname_size);
      song_descriptor_ptr->media_midi_filename_ptr[song_dirname_size] = '/';
      memcpy(song_descriptor_ptr->media_midi_filename_ptr + song_dirname_size + 1, media_filename_ptr, temp_size);
      song_descriptor_ptr->media_midi_filename_ptr[song_dirname_size + 1 + temp_size] = 0;

      path_normalize(song_descriptor_ptr->media_midi_filename_ptr);
    }
  }
  else
  {
    song_descriptor_ptr->media_midi_filename_ptr = NULL;
    NOTICE_OUT("MIDI absent");
  }

  if (audio != 0)
  {
    ret = xml_get_string_dup(doc_ptr, "/song/audio/media", &media_filename_ptr);
    if (ret != 0)
    {
      ERROR_OUT("Cannot find audio media filename within \"%s\"", song_filename_ptr);
      ret = -1;
      goto free_midi_filename;
    }

    NOTICE_OUT("Audio present");

    if (media_filename_ptr[0] == '/')
    {
      song_descriptor_ptr->media_audio_filename_ptr = media_filename_ptr;
    }
    else
    {
      temp_size = strlen(media_filename_ptr);

      song_descriptor_ptr->media_audio_filename_ptr = (char *)malloc(song_dirname_size + 1 + temp_size + 1);
      if (song_descriptor_ptr->media_audio_filename_ptr == NULL)
      {
        ERROR_OUT("malloc() failed.");
        ret = -1;
        goto exit_free_song_descriptor;
      }

      memcpy(song_descriptor_ptr->media_audio_filename_ptr, song_dirname_ptr, song_dirname_size);
      song_descriptor_ptr->media_audio_filename_ptr[song_dirname_size] = '/';
      memcpy(song_descriptor_ptr->media_audio_filename_ptr + song_dirname_size + 1, media_filename_ptr, temp_size);
      song_descriptor_ptr->media_audio_filename_ptr[song_dirname_size + 1 + temp_size] = 0;

      path_normalize(song_descriptor_ptr->media_audio_filename_ptr);
    }
  }
  else
  {
    song_descriptor_ptr->media_audio_filename_ptr = NULL;
    NOTICE_OUT("Audio absent");
  }

  if (video != 0)
  {
    ret = xml_get_string_dup(doc_ptr, "/song/video/media", &media_filename_ptr);
    if (ret != 0)
    {
      ERROR_OUT("Cannot find video media filename within \"%s\"", song_filename_ptr);
      ret = -1;
      goto free_audio_filename;
    }

    NOTICE_OUT("Video present");

    if (media_filename_ptr[0] == '/')
    {
      song_descriptor_ptr->media_video_filename_ptr = media_filename_ptr;
    }
    else
    {
      temp_size = strlen(media_filename_ptr);

      song_descriptor_ptr->media_video_filename_ptr = (char *)malloc(song_dirname_size + 1 + temp_size + 1);
      if (song_descriptor_ptr->media_video_filename_ptr == NULL)
      {
        ERROR_OUT("malloc() failed.");
        ret = -1;
        goto exit_free_song_descriptor;
      }

      memcpy(song_descriptor_ptr->media_video_filename_ptr, song_dirname_ptr, song_dirname_size);
      song_descriptor_ptr->media_video_filename_ptr[song_dirname_size] = '/';
      memcpy(song_descriptor_ptr->media_video_filename_ptr + song_dirname_size + 1, media_filename_ptr, temp_size);
      song_descriptor_ptr->media_video_filename_ptr[song_dirname_size + 1 + temp_size] = 0;

      path_normalize(song_descriptor_ptr->media_video_filename_ptr);
    }
  }
  else
  {
    song_descriptor_ptr->media_video_filename_ptr = NULL;
    NOTICE_OUT("Video absent");
  }

  ret = xml_get_string_dup(doc_ptr, XPATH_SONG_NAME_EXPRESSION, &song_descriptor_ptr->name_ptr);
  if (ret != 0)
  {
    ERROR_OUT("Cannot find song name within \"%s\"", song_filename_ptr);
    ret = -1;
    goto free_video_filename;
  }

  ret = xml_get_int(doc_ptr, "/song/start_offsets/video", &song_descriptor_ptr->start_offset_video);
  if (ret != 0)
  {
    DEBUG_OUT("song video start offset is not supplied, defaulting to 0");
    song_descriptor_ptr->start_offset_video = 0;
  }
  else
  {
    DEBUG_OUT("song video start offset is %d microseconds", song_descriptor_ptr->start_offset_video);
  }

  ret = xml_get_int(doc_ptr, "/song/start_offsets/audio", &song_descriptor_ptr->start_offset_audio);
  if (ret != 0)
  {
    DEBUG_OUT("song audio start offset is not supplied, defaulting to 0");
    song_descriptor_ptr->start_offset_audio = 0;
  }
  else
  {
    DEBUG_OUT("song audio start offset is %d microseconds", song_descriptor_ptr->start_offset_audio);
  }

  ret = xml_get_int(doc_ptr, "/song/start_offsets/midi", &song_descriptor_ptr->start_offset_midi);
  if (ret != 0)
  {
    DEBUG_OUT("song midi start offset is not supplied, defaulting to 0");
    song_descriptor_ptr->start_offset_midi = 0;
  }
  else
  {
    DEBUG_OUT("song midi start offset is %d microseconds", song_descriptor_ptr->start_offset_midi);
  }

  *song_descriptor_ptr_ptr = song_descriptor_ptr;
  ret = 0;
  goto free_doc;

free_video_filename:
  free(song_descriptor_ptr->media_video_filename_ptr);

free_audio_filename:
  free(song_descriptor_ptr->media_audio_filename_ptr);

free_midi_filename:
  free(song_descriptor_ptr->media_midi_filename_ptr);

exit_free_song_descriptor:
  free(song_descriptor_ptr);

free_doc:
  xmlFreeDoc(doc_ptr);

free_song_copy:
  free(song_filename_copy_ptr);

exit:
  return ret;
}

int
play_song(
  const char * song_filename_ptr,
  eos_t eos)
{
  int ret, ret1;
  int start_offsets[3];
  struct player * players[3];
  int i;
  int j;
  struct player * player_temp_ptr;
  int start_offset_temp;
  int global_start_offset_video;
  int global_start_offset_audio;
  int global_start_offset_midi;

  DEBUG_OUT("play_song(%s) called.", song_filename_ptr);

  ret = conf_file_get_int("/conf/start_offsets/video", &global_start_offset_video);
  if (ret != 0)
  {
    ERROR_OUT("failed to read from configuration what global video start offset to use");
    ret = -1;
    goto exit;
  }

  ret = conf_file_get_int("/conf/start_offsets/audio", &global_start_offset_audio);
  if (ret != 0)
  {
    ERROR_OUT("failed to read from configuration what global audio start offset to use");
    ret = -1;
    goto exit;
  }

  ret = conf_file_get_int("/conf/start_offsets/midi", &global_start_offset_midi);
  if (ret != 0)
  {
    ERROR_OUT("failed to read from configuration what global midi start offset to use");
    ret = -1;
    goto exit;
  }

  DEBUG_OUT("Using global video start offset of %d microseconds", global_start_offset_video);
  DEBUG_OUT("Using global audio start offset of %d microseconds", global_start_offset_audio);
  DEBUG_OUT("Using global midi start offset of %d microseconds", global_start_offset_midi);

  ret = get_song_descriptor(song_filename_ptr, &g_song_descriptor_ptr);
  if (ret != 0)
  {
    ERROR_OUT("Failed to get song descriptor (%d)", ret);
    ret = -1;
    goto exit;
  }

  OUTPUT_MESSAGE("====== Play \"%s\"", g_song_descriptor_ptr->name_ptr);

  dump_song_descriptor(g_song_descriptor_ptr);

  DEBUG_OUT("Initializing...");

  g_real_mask = 0;

  if (g_song_descriptor_ptr->media_video_filename_ptr != NULL)
  {
    g_real_mask |= EOD_CONTEXT_VIDEO;
  }

  if (g_song_descriptor_ptr->media_audio_filename_ptr != NULL)
  {
    g_real_mask |= EOD_CONTEXT_AUDIO;
  }

  if (g_song_descriptor_ptr->media_midi_filename_ptr != NULL)
  {
    g_real_mask |= EOD_CONTEXT_MIDI;
  }

  ret = pthread_mutex_init(&g_real_mask_mutex, NULL);
  if (ret != 0)
  {
    ERROR_OUT("Failed to init real mask mutex (%d)", ret);
    ret = -1;
    goto exit_free_song_descriptor;
  }

  if (g_song_descriptor_ptr->media_video_filename_ptr != NULL)
  {
    ret = media_source_dv(g_song_descriptor_ptr->media_video_filename_ptr, &g_media_source_dv_ptr);
    if (ret < 0)
    {
      ERROR_OUT("failed to create the DV source");
      ret = 1;
      goto exit_destroy_mutex;
    }
  }

  if (g_song_descriptor_ptr->media_audio_filename_ptr != NULL)
  {
    ret = media_source_wav(g_song_descriptor_ptr->media_audio_filename_ptr, &g_media_source_wav_ptr);
    if (ret < 0)
    {
      ERROR_OUT("failed to create the WAV source");
      ret = 1;
      goto exit_destroy_media_source_dv;
    }
  }

  if (g_song_descriptor_ptr->media_midi_filename_ptr != NULL)
  {
    ret = media_source_midi(g_song_descriptor_ptr->media_midi_filename_ptr, &g_media_source_midi_ptr);
    if (ret < 0)
    {
      ERROR_OUT("failed to create the MIDI source");
      ret = 1;
      goto exit_destroy_media_source_wav;
    }
  }

  if (g_song_descriptor_ptr->media_video_filename_ptr != NULL)
  {
    ret = player_video1394(g_media_source_dv_ptr, eod, EOD_CONTEXT_VIDEO, &g_player_video1394_ptr);
    if (ret < 0)
    {
      ERROR_OUT("failed to create the firewire player");
      ret = 1;
      goto exit_destroy_media_source_midi;
    }
  }
  else
  {
    g_player_video1394_ptr = NULL;
  }

  if (g_song_descriptor_ptr->media_audio_filename_ptr != NULL)
  {
    ret = player_alsa_pcm(g_media_source_wav_ptr, eod, EOD_CONTEXT_AUDIO, &g_player_alsa_pcm_ptr);
    if (ret < 0)
    {
      ERROR_OUT("failed to create the ALSA PCM player");
      ret = 1;
      goto exit_destroy_video_1394_player;
    }
  }
  else
  {
    g_player_alsa_pcm_ptr = NULL;
  }

  if (g_song_descriptor_ptr->media_midi_filename_ptr != NULL)
  {
    ret = player_alsa_midi(g_media_source_midi_ptr, eod, EOD_CONTEXT_MIDI, &g_player_alsa_midi_ptr);
    if (ret < 0)
    {
      ERROR_OUT("failed to create the ALSA MIDI player");
      ret = 1;
      goto exit_destroy_alsa_pcm_player;
    }
  }
  else
  {
    g_player_alsa_midi_ptr = NULL;
  }

  players[0] = g_player_video1394_ptr;
  start_offsets[0] = global_start_offset_video + g_song_descriptor_ptr->start_offset_video;

  players[1] = g_player_alsa_pcm_ptr;
  start_offsets[1] = global_start_offset_audio + g_song_descriptor_ptr->start_offset_audio;

  players[2] = g_player_alsa_midi_ptr;
  start_offsets[2] = global_start_offset_midi + g_song_descriptor_ptr->start_offset_midi;

  DEBUG_OUT("Preparing...");

  for (i = 0 ; i < 3 ; i++)
  {
    //ERROR_OUT("Preparing %s player", players[i]->name);
    if (players[i] != NULL)
    {
      ret = player_start_prepare(players[i]);
      if (ret < 0)
      {
        ERROR_OUT("failed to prepare %s player for start", players[i]->name);
        ret = 1;
        goto exit_destroy_players;
      }
    }
  }

  /* sort players by start offset */
  for (i = 0 ; i < 3 ; i++)
  {
    for (j = i + 1 ; j < 3 ; j++)
    {
      //DEBUG_OUT("Comparing %i (%i) and %i (%i)", j, start_offsets[j], i, start_offsets[i]);
      if (start_offsets[j] < start_offsets[i])
      {
        //DEBUG_OUT("Exchanging %i and %i", i, j);
        start_offset_temp = start_offsets[i];
        player_temp_ptr = players[i];

        start_offsets[i] = start_offsets[j];
        players[i] = players[j];

        start_offsets[j] = start_offset_temp;
        players[j] = player_temp_ptr;
      }
    }
  }

/*   DEBUG_OUT("------ sorted players"); */
/*   for (i = 0 ; i < 3 ; i++) */
/*   { */
/*     DEBUG_OUT("%s player, start offset %d", players[i]->name, start_offsets[i]); */
/*   } */

  /* normalize offsets and make start offsets relative */
  start_offset_temp = start_offsets[0];
  for (i = 0 ; i < 3 ; i++)
  {
    start_offsets[i] -= start_offset_temp;
  }

  for (i = 1 ; i < 3 ; i++)
  {
    start_offsets[i] -= start_offsets[i-1];
  }

/*   DEBUG_OUT("------ normalized players"); */
/*   for (i = 0 ; i < 3 ; i++) */
/*   { */
/*     DEBUG_OUT("%s player, start offset %d", players[i]->name, start_offsets[i]); */
/*   } */

  /* show startup plan */
  DEBUG_OUT("------ players startup plan");
  for (i = 0 ; i < 3 ; i++)
  {
    if (start_offsets[i] != 0)
    {
      DEBUG_OUT("Wait %d microseconds", start_offsets[i]);
      if (start_offsets[i] < 0)
      {
        ERROR_OUT("Cannot reverse the time (sort and/or normalize failed)");
        goto exit_destroy_players;
      }
    }

    if (players[i] != NULL)
    {
      DEBUG_OUT("Start %s player", players[i]->name);
    }
  }

  g_eos = eos;

  OUTPUT_MESSAGE("...... GO!");
  for (i = 0 ; i < 3 ; i++)
  {
    if (start_offsets[i] != 0)
    {
      //DEBUG_OUT("Waiting %d microseconds", start_offsets[i]);
      usleep(start_offsets[i]);
    }

    if (players[i] != NULL)
    {
      //DEBUG_OUT("Starting %s player", players[i]->name);
      ret = player_start(players[i]);
      if (ret < 0)
      {
        ERROR_OUT("failed to start %s player", players[i]->name);
        ret = 1;
        goto exit_destroy_players;
      }
    }
  }

  ret = 0;
  goto exit;

exit_destroy_players:
//exit_destroy_alsa_midi_player:
  if (g_song_descriptor_ptr->media_midi_filename_ptr != NULL)
  {
    player_destroy(g_player_alsa_midi_ptr);
  }

exit_destroy_alsa_pcm_player:
  if (g_song_descriptor_ptr->media_audio_filename_ptr != NULL)
  {
    player_destroy(g_player_alsa_pcm_ptr);
  }

exit_destroy_video_1394_player:
  if (g_song_descriptor_ptr->media_video_filename_ptr != NULL)
  {
    player_destroy(g_player_video1394_ptr);
  }

exit_destroy_media_source_midi:
  if (g_song_descriptor_ptr->media_midi_filename_ptr != NULL)
  {
    media_source_destroy(g_media_source_midi_ptr);
  }

exit_destroy_media_source_wav:
  if (g_song_descriptor_ptr->media_audio_filename_ptr != NULL)
  {
    media_source_destroy(g_media_source_wav_ptr);
  }

exit_destroy_media_source_dv:
  if (g_song_descriptor_ptr->media_video_filename_ptr != NULL)
  {
    media_source_destroy(g_media_source_dv_ptr);
  }

exit_destroy_mutex:
  ret1 = pthread_mutex_destroy(&g_real_mask_mutex);
  if (ret1 != 0)
  {
    ERROR_OUT("Failed to destroy real mask mutex (%d)", ret1);
  }

exit_free_song_descriptor:
  free_song_descriptor(g_song_descriptor_ptr);

exit:
  return ret;
}

void
end_song()
{
  DEBUG_OUT("------ STOP!");

  pthread_mutex_lock(&g_real_mask_mutex);
  if (g_real_mask != 0)
  {
    OUTPUT_MESSAGE("====== Stopping...");
  }
  pthread_mutex_unlock(&g_real_mask_mutex);

  if (g_song_descriptor_ptr->media_midi_filename_ptr != NULL)
    player_destroy(g_player_alsa_midi_ptr);

  if (g_song_descriptor_ptr->media_audio_filename_ptr != NULL)
    player_destroy(g_player_alsa_pcm_ptr);

  if (g_song_descriptor_ptr->media_video_filename_ptr != NULL)
    player_destroy(g_player_video1394_ptr);

  if (g_song_descriptor_ptr->media_midi_filename_ptr != NULL)
    media_source_destroy(g_media_source_midi_ptr);

  if (g_song_descriptor_ptr->media_audio_filename_ptr != NULL)
    media_source_destroy(g_media_source_wav_ptr);

  if (g_song_descriptor_ptr->media_video_filename_ptr != NULL)
    media_source_destroy(g_media_source_dv_ptr);

  pthread_mutex_destroy(&g_real_mask_mutex);

  free_song_descriptor(g_song_descriptor_ptr);

  g_player_alsa_midi_ptr = NULL;
  g_player_alsa_pcm_ptr = NULL;
  g_player_video1394_ptr = NULL;
  g_media_source_midi_ptr = NULL;
  g_media_source_wav_ptr = NULL;
  g_media_source_dv_ptr = NULL;
  g_song_descriptor_ptr = NULL;

  if (g_real_mask != 0)
  {
    OUTPUT_MESSAGE("====== Stopped");
  }
}
