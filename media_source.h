/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 *   Media source abstraction
 *   This file is part of monster
 *
 *   Copyright (C) 2006,2007 Nedko Arnaudov <nedko@arnaudov.name>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; version 2 of the License
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *****************************************************************************/

#ifndef MEDIA_SOURCE_H__BBE1F2DC_F33E_4A66_83A1_FC07019C7010__INCLUDED
#define MEDIA_SOURCE_H__BBE1F2DC_F33E_4A66_83A1_FC07019C7010__INCLUDED

#define MEDIA_FORMAT_DV_PAL             1 /* DV 625/50, 144000 bytes frame size */
#define MEDIA_FORMAT_DV_NTSC            2 /* DV 525/60, 120000 bytes frame size */
#define MEDIA_FORMAT_PCM_16_STEREO_44   3 /* PCM, 16-bit, stereo, 44.1 Khz */
#define MEDIA_FORMAT_PCM_16_STEREO_48   4 /* PCM, 16-bit, stereo, 48 Khz */
#define MEDIA_FORMAT_MIDI_EVENT         5 /* midi_event.h */

struct media_source;

typedef void (* media_source_destroy_t)(struct media_source * media_source_ptr);

typedef int (* media_source_format_query_t)(struct media_source * media_source_ptr, unsigned int * format_ptr);

/* negative - error, 0 - regular data, 1 - EOF fake data */
typedef int (* media_source_get_frame_data_t)(struct media_source * media_source_ptr, void ** buffer_ptr_ptr);

typedef void (* media_source_next_frame_t)(struct media_source * media_source_ptr);

struct media_source
{
  media_source_destroy_t destroy;
  media_source_format_query_t format_query;
  media_source_get_frame_data_t get_frame_data;
  media_source_next_frame_t next_frame;
};

static __inline__
void
media_source_destroy(struct media_source * media_source_ptr)
{
  return media_source_ptr->destroy(media_source_ptr);
};

static __inline__
int
media_source_format_query(struct media_source * media_source_ptr, unsigned int * format_ptr)
{
  return media_source_ptr->format_query(media_source_ptr, format_ptr);
};

static __inline__
int
media_source_get_frame_data(struct media_source * media_source_ptr, void ** buffer_ptr_ptr)
{
  return media_source_ptr->get_frame_data(media_source_ptr, buffer_ptr_ptr);
};

static __inline__
void
media_source_next_frame(struct media_source * media_source_ptr)
{
  media_source_ptr->next_frame(media_source_ptr);
};

#endif /* #ifndef MEDIA_SOURCE_H__BBE1F2DC_F33E_4A66_83A1_FC07019C7010__INCLUDED */
