/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 *   Playback control and playlist handling
 *   This file is part of monster
 *
 *   Copyright (C) 2006,2007 Nedko Arnaudov <nedko@arnaudov.name>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; version 2 of the License
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *****************************************************************************/

#ifndef PLAYBACK_H__6CB9696F_CF93_490A_A1B5_A4AC72FD54F0__INCLUDED
#define PLAYBACK_H__6CB9696F_CF93_490A_A1B5_A4AC72FD54F0__INCLUDED

int
playback_init(const char * playlist_filename_ptr);

void
playlist_display();

void
playback_toggle();

void
playback_next();

void
playback_prev();

void
playback_destroy();

void
playback_goto(unsigned int song);

#endif /* #ifndef PLAYBACK_H__6CB9696F_CF93_490A_A1B5_A4AC72FD54F0__INCLUDED */
