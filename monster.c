/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 *   monster main function and keyboard processing
 *   This file is part of monster
 *
 *   Copyright (C) 2006,2007 Nedko Arnaudov <nedko@arnaudov.name>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; version 2 of the License
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *****************************************************************************/

#include <sys/mman.h>
#include <libxml/parser.h>
#include <curses.h>
#include <signal.h>

#define DISABLE_DEBUG_OUTPUT

#include "log.h"
#include "conf.h"
#include "playback.h"

int
main(int argc, char ** argv)
{
  int ret;
  char c;
  char goto_buffer[100];
  int goto_index;

  if (argc != 2)
  {
    fprintf(stderr, "Syntax: monster <playlist>\n");
    ret = 1;
    goto exit;
  }

  if (mlockall(MCL_CURRENT | MCL_FUTURE) == -1)
  {
    ERROR_OUT("Cannot disable memory swapping");
    ret = 1;
    goto exit;
  }

  initscr();
  cbreak();
  noecho();
  raw();
  nodelay(stdscr, 0);
  erase();

  refresh();

  NOTICE_OUT("****************************** Monster started for \"%s\"", argv[1]);

  /* Init libxml */     
  xmlInitParser();

  /*
   * this initialize the library and check potential ABI mismatches
   * between the version it was compiled for and the actual shared
   * library used.
   */
  LIBXML_TEST_VERSION;

  ret = conf_file_parse();
  if (ret != 0)
  {
    ERROR_OUT("Failed to parse conf file (%d)", ret);
    ret = -1;
    goto exit_xml_cleanup_parser;
  }

  ret = playback_init(argv[1]);
  if (ret != 0)
  {
    ERROR_OUT("playback() failed.");
    ret = -1;
    goto exit_conf_cleanup;
  }

loop:
  c = getch();

  NOTICE_OUT("'%u'", c);

  switch (c)
  {
  case 3:                    /* break char */
  case 'q':
    OUTPUT_MESSAGE("---------------- QUIT ----------------");
    ret = 0;
    goto exit_playback_destroy;
  case ' ':                     /* space */
    OUTPUT_MESSAGE("---------- PLAYBACK TOGGLE -----------");
    playback_toggle();
    break;
  case '.':                     /* '>' */
    OUTPUT_MESSAGE("---------------- NEXT ----------------");
    playback_next();
    break;
  case ',':                     /* '<' */
    OUTPUT_MESSAGE("---------------- PREV ----------------");
    playback_prev();
    break;
  case 'P':
  case 'p':
    playlist_display();
    break;
  case '0':
    OUTPUT_MESSAGE("---------------- GOTO 10  ----------------");
    playback_goto(10);
    break;
  case '1':
  case '2':
  case '3':
  case '4':
  case '5':
  case '6':
  case '7':
  case '8':
  case '9':
    OUTPUT_MESSAGE("---------------- GOTO %u  ----------------", c - '0');
    playback_goto(c - '0');
    break;
  case '\n':
    OUTPUT_MESSAGE("---------------- GOTO  ----------------");
    OUTPUT_MESSAGE("Enter digits and pres Enter key again");
    goto_index = 0;
  loop_goto:
    c = getch();
    if (c == '\n')
    {
      goto_buffer[goto_index] = 0;
      if (goto_index == 0)
      {
        ERROR_OUT("No digits!");
        OUTPUT_MESSAGE("---------------- Canceling GOTO  ----------------");
        break;
      }

      if (atoi(goto_buffer) == 0)
      {
        ERROR_OUT("Zero song! Are you crazy or what?!?!");
        OUTPUT_MESSAGE("---------------- Canceling GOTO  ----------------");
        break;
      }

      OUTPUT_MESSAGE("goto %s", goto_buffer);
      playback_goto(atoi(goto_buffer));
      break;
    }
    else if (c >= '0' && c <= '9')
    {
      if (goto_index == sizeof(goto_buffer) - 1)
      {
        ERROR_OUT("Too many digits! Are you crazy or what?!?!");
        OUTPUT_MESSAGE("---------------- Canceling GOTO  ----------------");
        break;
      }

      goto_buffer[goto_index] = c;
      goto_index++;
      goto_buffer[goto_index] = 0;
      NOTICE_OUT("%s...", goto_buffer);
    }
    else
    {
      ERROR_OUT("Please enter DIGITS!");
    }

    goto loop_goto;
  }

  goto loop;

exit_playback_destroy:
  playback_destroy();

exit_conf_cleanup:
  conf_file_cleanup();

exit_xml_cleanup_parser:
  /* XML library cleanup. */
  xmlCleanupParser();

  if (ret != 0)
  {
    getch();
  }

  endwin();

  NOTICE_OUT("****************************** Monster stopped.");

exit:
  return ret;
}
