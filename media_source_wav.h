/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 *   Initialization of media source implementation that reads WAV files.
 *   This file is part of monster
 *
 *   Copyright (C) 2006,2007 Nedko Arnaudov <nedko@arnaudov.name>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; version 2 of the License
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *****************************************************************************/

#ifndef MEDIA_SOURCE_WAV_H__44455E4D_8D77_488B_AB7C_4165AD9EB33F__INCLUDED
#define MEDIA_SOURCE_WAV_H__44455E4D_8D77_488B_AB7C_4165AD9EB33F__INCLUDED

int
media_source_wav(
  const char * filename,
  struct media_source ** media_source_ptr_ptr);

#endif /* #ifndef MEDIA_SOURCE_WAV_H__44455E4D_8D77_488B_AB7C_4165AD9EB33F__INCLUDED */
