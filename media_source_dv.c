/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 *   Media source implementation that reads DV frames from raw DV files.
 *   This file is part of monster
 *
 *   Copyright (C) 2006,2007 Nedko Arnaudov <nedko@arnaudov.name>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; version 2 of the License
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *****************************************************************************/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>

#define DISABLE_DEBUG_OUTPUT

#include "log.h"
#include "media_source.h"
#include "media_source_dv.h"
#include "types.h"
#include "dv.h"
#include "conf.h"

struct media_source_dv
{
  struct media_source virtual;
  int file_handle;
  unsigned int format;
  unsigned int frame_size;
  void * frame_buffer_ptr;
  int error;
  bool eof;
  int initial_repeat;
  int initial_counter;
};

#define media_source_ptr ((struct media_source_dv *)virtual_ptr)

void media_source_dv_destroy(struct media_source * virtual_ptr)
{
  DEBUG_OUT("media_source_dv_destroy");
  close(media_source_ptr->file_handle);
  free(media_source_ptr->frame_buffer_ptr);
  free(media_source_ptr);
}

int media_source_dv_format_query(struct media_source * virtual_ptr, unsigned int * format_ptr)
{
  DEBUG_OUT("media_source_dv_format_query");
  *format_ptr = media_source_ptr->format;
  return 0;
}

int media_source_dv_get_frame_data(struct media_source * virtual_ptr, void ** buffer_ptr_ptr)
{
  DEBUG_OUT("media_source_dv_get_frame_data");

  if (media_source_ptr->error < 0)
  {
    return media_source_ptr->error;
  }

  *buffer_ptr_ptr = media_source_ptr->frame_buffer_ptr;

  return media_source_ptr->eof ? 1 : 0;
}

void
media_source_dv_next_frame(struct media_source * virtual_ptr)
{
  ssize_t ssret;

  DEBUG_OUT("media_source_dv_next_frame");

  if (media_source_ptr->error < 0)
  {
    return;
  }

  if (media_source_ptr->initial_counter == 0)
  {
    media_source_ptr->initial_counter++;
//    memset(media_source_ptr->frame_buffer_ptr, 0, media_source_ptr->frame_size);
//    return;
  }
  else if (media_source_ptr->initial_counter < media_source_ptr->initial_repeat)
  {
    media_source_ptr->initial_counter++;
    return;
  }

  ssret = read(media_source_ptr->file_handle, media_source_ptr->frame_buffer_ptr, media_source_ptr->frame_size);
  if (ssret != media_source_ptr->frame_size)
  {
    if (ssret == -1)
    {
      ERROR_OUT("cannot read the raw DV file (%d)", errno);
      media_source_ptr->error = -1;
    }
    else if (ssret != 0)
    {
      ERROR_OUT("only %u bytes read from the raw DV file instead of %u", (unsigned int)ssret, media_source_ptr->frame_size);
      media_source_ptr->error = -1;
    }
    else
    {
      DEBUG_OUT("no more data");
      media_source_ptr->eof = true;
    }
  }
}

#undef media_source_ptr

int
media_source_dv(
  const char * filename,
  struct media_source ** media_source_ptr_ptr)
{
  struct media_source_dv * media_source_ptr;
  int file_handle;
  struct stat st;
  byte data[4];
  int ret;
  unsigned int format;
  unsigned int frame_size;
  ssize_t ssret;
  unsigned int calibration_frames;

  DEBUG_OUT("media_source_dv");

  ret = conf_file_get_uint("/conf/video/calibration_frames", &calibration_frames);
  if (ret != 0)
  {
    ERROR_OUT("failed to read from configuration how many calibration frames to use");
    ret = -1;
    goto exit;
  }

  file_handle = open(filename, O_LARGEFILE);
  if (file_handle == -1)
  {
    ERROR_OUT("cannot open \"%s\" (%d)", filename, errno);
    ret = -1;
    goto exit;
  }

  if (fstat(file_handle, &st) == -1)
  {
    ERROR_OUT("cannot fstat \"%s\" (%d)", filename, errno);
    ret = -1;
    goto exit_close_handle;
  }

  if (st.st_size < 4)
  {
    ERROR_OUT("\"%s\" is too small", filename);
    ret = -1;
    goto exit_close_handle;
  }

  /* read header */
  ssret = read(file_handle, data, 4);
  if (ssret != 4)
  {
    if (ssret == -1)
    {
      ERROR_OUT("cannot read \"%s\" (%d)", filename, errno);
    }
    else
    {
      ERROR_OUT("only %u bytes read from \"%s\" instead of 4", (unsigned int)ssret, filename);
    }
    ret = -1;
    goto exit_close_handle;
  }

  if ((data[0] >> 5) != 0 ||    /* first dif block should be header section */
      (data[3] & 0x40) != 0)    /* this bit must be for DV files */
  {
    ERROR_OUT("\"%s\" is not in raw DV format (header check)", filename);
    ret = -1;
    goto exit_close_handle;
  }

  if ((data[3] & 0x80) != 0)
  {
    DEBUG_OUT("625/50 (PAL)");
    format = MEDIA_FORMAT_DV_PAL;
    frame_size = DV_FRAME_SIZE_PAL;
    NOTICE_OUT("Video: Using %u calibration frames (%f seconds)", calibration_frames, (float)calibration_frames/25.0);
  }
  else
  {
    DEBUG_OUT("525/60 (NTSC)");
    format = MEDIA_FORMAT_DV_NTSC;
    frame_size = DV_FRAME_SIZE_NTSC;
    NOTICE_OUT("Video: Using %u calibration frames (%f seconds)", calibration_frames, (float)calibration_frames/29.97);
  }

  if (st.st_size % frame_size != 0)
  {
    ERROR_OUT("\"%s\" is not in raw DV format (size check)", filename);
    ret = -1;
    goto exit_close_handle;
  }

  /* rewind */
  if (lseek(file_handle, 0, SEEK_SET) != 0)
  {
    ERROR_OUT("\"%s\" rewind failed", filename);
    ret = -1;
    goto exit_close_handle;
  }

  media_source_ptr = (struct media_source_dv *)malloc(sizeof(struct media_source_dv));
  if (media_source_ptr == NULL)
  {
    ERROR_OUT("malloc() failed.");
    ret = -1;
    goto exit_close_handle;
  }

  media_source_ptr->frame_buffer_ptr = malloc(frame_size);
  if (media_source_ptr->frame_buffer_ptr == NULL)
  {
    ERROR_OUT("malloc() failed.");
    ret = -1;
    goto exit_free_ms;
  }

  media_source_ptr->virtual.destroy = media_source_dv_destroy;
  media_source_ptr->virtual.format_query = media_source_dv_format_query;
  media_source_ptr->virtual.get_frame_data = media_source_dv_get_frame_data;
  media_source_ptr->virtual.next_frame = media_source_dv_next_frame;

  media_source_ptr->file_handle = file_handle;
  media_source_ptr->format = format;
  media_source_ptr->frame_size = frame_size;

  media_source_ptr->error = 0;
  media_source_ptr->eof = false;
  media_source_ptr->initial_counter = 0;
  media_source_ptr->initial_repeat = calibration_frames;
  media_source_dv_next_frame(&media_source_ptr->virtual);

  if (media_source_ptr->error < 0)
  {
    ret = media_source_ptr->error;
    goto exit_free_frame_data;
  }

  *media_source_ptr_ptr = &media_source_ptr->virtual;

  ret = 0;
  goto exit;

exit_free_frame_data:
  free(media_source_ptr->frame_buffer_ptr);

exit_free_ms:
  free(media_source_ptr);

exit_close_handle:
  close(file_handle);

exit:
  return ret;
}
