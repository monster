/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 *   Media source implementation that reads WAV files.
 *   This file is part of monster
 *
 *   Copyright (C) 2006,2007 Nedko Arnaudov <nedko@arnaudov.name>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; version 2 of the License
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *****************************************************************************/

#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <inttypes.h>

#define DISABLE_DEBUG_OUTPUT

#include "media_source.h"
#include "media_source_wav.h"
#include "log.h"
#include "types.h"
#include "wav.h"

struct media_source_wav
{
  struct media_source virtual;
  int file_handle;
  unsigned int format;
  unsigned int size_left;
  char chunk_buffer[WAV_DATA_STEREO_16];
  int error;
};

#define media_source_ptr ((struct media_source_wav *)virtual_ptr)

void media_source_wav_destroy(struct media_source * virtual_ptr)
{
  DEBUG_OUT("media_source_wav_destroy");
  close(media_source_ptr->file_handle);
  free(media_source_ptr);
}

int media_source_wav_format_query(struct media_source * virtual_ptr, unsigned int * format_ptr)
{
  DEBUG_OUT("media_source_wav_format_query");
  *format_ptr = media_source_ptr->format;
  return 0;
}

int media_source_wav_get_frame_data(struct media_source * virtual_ptr, void ** buffer_ptr_ptr)
{
  DEBUG_OUT("media_source_wav_get_frame_data");

  if (media_source_ptr->error < 0)
  {
    return media_source_ptr->error;
  }

  *buffer_ptr_ptr = media_source_ptr->chunk_buffer;

  return media_source_ptr->size_left ? 0 : 1;
}

void
media_source_wav_next_frame(struct media_source * virtual_ptr)
{
  ssize_t ssret;
  size_t read_size;

  DEBUG_OUT("media_source_wav_next_frame");

  if (media_source_ptr->error < 0)
  {
    return;
  }

  read_size = WAV_DATA_STEREO_16;
  if (read_size > media_source_ptr->size_left)
  {
    read_size = media_source_ptr->size_left;
  }

  if (read_size != 0)
  {
    ssret = read(media_source_ptr->file_handle, media_source_ptr->chunk_buffer, read_size);
    if (ssret == -1)
    {
      ERROR_OUT("cannot read the wav file (%d)", errno);
      media_source_ptr->error = -1;
      return;
    }

    if (ssret != read_size)
    {
      ERROR_OUT("only %u bytes read from the wav file instead of %u", (unsigned int)ssret, (unsigned int)read_size);
      media_source_ptr->error = -1;
      return;
    }
  }

  media_source_ptr->size_left -= read_size;

  if (ssret < WAV_DATA_STEREO_16)
  {
    memset(media_source_ptr->chunk_buffer + read_size, 0, WAV_DATA_STEREO_16 - read_size);
  }
}

#undef media_source_ptr

int
media_source_wav(
  const char * filename,
  struct media_source ** media_source_ptr_ptr)
{
  struct media_source_wav * media_source_ptr;
  int file_handle;
  struct stat st;
  int ret;
  struct wave_cannonical_header header;
  struct wave_chunk_header chunk_header;
  ssize_t ssret;
  size_t skip;

  DEBUG_OUT("media_source_wav");

  file_handle = open(filename, O_LARGEFILE);
  if (file_handle == -1)
  {
    ERROR_OUT("cannot open \"%s\" (%d)", filename, errno);
    ret = -1;
    goto exit;
  }

  if (fstat(file_handle, &st) == -1)
  {
    ERROR_OUT("cannot fstat \"%s\" (%d)", filename, errno);
    ret = -1;
    goto exit_close_handle;
  }

  if (st.st_size < sizeof(struct wave_cannonical_header))
  {
    ERROR_OUT("\"%s\" is too small", filename);
    ret = -1;
    goto exit_close_handle;
  }

  DEBUG_OUT("%s with size %zu", filename, (size_t)st.st_size);

  /* read header */
  ssret = read(file_handle, &header, sizeof(struct wave_cannonical_header));
  if (ssret != sizeof(struct wave_cannonical_header))
  {
    if (ssret == -1)
    {
      ERROR_OUT("cannot read \"%s\" (%d)", filename, errno);
    }
    else
    {
      ERROR_OUT("only %u bytes read from \"%s\" instead of %u (wave cannonical header)", (unsigned int)ssret, filename, (unsigned int)sizeof(struct wave_cannonical_header));
    }
    ret = -1;
    goto exit_close_handle;
  }

  if (header.riff_header.magic != WAV_RIFF ||
      header.riff_header.type != WAV_WAVE ||
      header.fmt_header.type != WAV_FMT)
  {
    ERROR_OUT("\"%s\" is not in canonical WAV format (header check)", filename);
    ret = -1;
    goto exit_close_handle;
  }

  header.fmt_header.length = LE_32(header.fmt_header.length);
  if (header.fmt_header.length > sizeof(struct wave_fmt_body))
  {
    skip = header.fmt_header.length - sizeof(struct wave_fmt_body);

    DEBUG_OUT("%zu bytes have to be skipped", skip);
    if (lseek(file_handle, skip, SEEK_CUR) == -1)
    {
      ERROR_OUT("cannot seek \"%s\" (%d)", filename, errno);
      ret = -1;
      goto exit_close_handle;
    }
  }

  /* Search for data chunk */
data_chunk_search_loop:
  ssret = read(file_handle, &chunk_header, sizeof(struct wave_chunk_header));
  if (ssret != sizeof(struct wave_chunk_header))
  {
    if (ssret == -1)
    {
      ERROR_OUT("cannot read \"%s\" (%d)", filename, errno);
    }
    else
    {
      ERROR_OUT("only %u bytes read from \"%s\" instead of %u (wave chunk header)", (unsigned int)ssret, filename, (unsigned int)sizeof(struct wave_chunk_header));
    }
    ret = -1;
    goto exit_close_handle;
  }

  DEBUG_OUT("wav chunk of type 0x%"PRIX32" with size %"PRIu32" bytes", chunk_header.type, chunk_header.length);

  if (chunk_header.type != WAV_DATA)
  {
    if (lseek(file_handle, LE_32(chunk_header.length), SEEK_CUR) == -1)
    {
      ERROR_OUT("cannot seek \"%s\" (%d)", filename, errno);
      ret = -1;
      goto exit_close_handle;
    }

    goto data_chunk_search_loop;
  }

  header.fmt_body.format = LE_16(header.fmt_body.format);
  header.fmt_body.modus = LE_16(header.fmt_body.modus);
  header.fmt_body.sample_fq = LE_32(header.fmt_body.sample_fq);
  header.fmt_body.byte_p_sec = LE_32(header.fmt_body.byte_p_sec);
  header.fmt_body.byte_p_spl = LE_16(header.fmt_body.byte_p_spl);
  header.fmt_body.bit_p_spl = LE_16(header.fmt_body.bit_p_spl);

  if (header.fmt_body.format != 1)
  {
    ERROR_OUT("\"%s\" is not in uncompressed PCM", filename);
    ret = -1;
    goto exit_close_handle;
  }

  if (header.fmt_body.modus != 2)
  {
    ERROR_OUT("\"%s\" is not stereo, it is %u channel audio", filename, (unsigned int)header.fmt_body.modus);
    ret = -1;
    goto exit_close_handle;
  }

  if (header.fmt_body.sample_fq != 48000 &&
      header.fmt_body.sample_fq != 44100)
  {
    ERROR_OUT("\"%s\" has unsupported sample rate of %u", filename, (unsigned int)header.fmt_body.sample_fq);
    ret = -1;
    goto exit_close_handle;
  }

  if (header.fmt_body.bit_p_spl != 16)
  {
    ERROR_OUT("\"%s\" has unsupported bits-per-sample = %u", filename, (unsigned int)header.fmt_body.bit_p_spl);
    ret = -1;
    goto exit_close_handle;
  }

  DEBUG_OUT("---- WAV file: %s", filename);
  DEBUG_OUT("Sample rate: %u Hz", (unsigned int)header.fmt_body.sample_fq);
  DEBUG_OUT("------------");

  media_source_ptr = (struct media_source_wav *)malloc(sizeof(struct media_source_wav));
  if (media_source_ptr == NULL)
  {
    ERROR_OUT("malloc() failed.");
    ret = -1;
    goto exit_close_handle;
  }

  media_source_ptr->virtual.destroy = media_source_wav_destroy;
  media_source_ptr->virtual.format_query = media_source_wav_format_query;
  media_source_ptr->virtual.get_frame_data = media_source_wav_get_frame_data;
  media_source_ptr->virtual.next_frame = media_source_wav_next_frame;

  media_source_ptr->file_handle = file_handle;
  media_source_ptr->format = (header.fmt_body.sample_fq == 48000) ? MEDIA_FORMAT_PCM_16_STEREO_48 : MEDIA_FORMAT_PCM_16_STEREO_44;
  media_source_ptr->size_left = LE_32(chunk_header.length);
  
  //media_source_ptr->size_left = WAV_DATA_STEREO_16 * 50;

  media_source_ptr->error = 0;
  media_source_wav_next_frame(&media_source_ptr->virtual);

  if (media_source_ptr->error < 0)
  {
    ret = media_source_ptr->error;
    goto exit_free_ms;
  }

  *media_source_ptr_ptr = &media_source_ptr->virtual;

  ret = 0;
  goto exit;

exit_free_ms:
  free(media_source_ptr);

exit_close_handle:
  close(file_handle);

exit:
  return ret;
}
