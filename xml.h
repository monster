/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 *   XML parsing helpers
 *
 *   Copyright (C) 2006,2007 Nedko Arnaudov <nedko@arnaudov.name>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; version 2 of the License
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *****************************************************************************/

#ifndef XML_H__FB295C79_9892_4896_9C01_F5EE8F0A2482__INCLUDED
#define XML_H__FB295C79_9892_4896_9C01_F5EE8F0A2482__INCLUDED

int
xml_get_string(
  xmlDocPtr doc_ptr,
  const char * xpath,
  char * buffer_ptr,
  size_t buffer_size);

int
xml_get_string_dup(
  xmlDocPtr doc_ptr,
  const char * xpath,
  char ** buffer_ptr_ptr);

int
xml_get_int(
  xmlDocPtr doc_ptr,
  const char * xpath,
  int * value_ptr);

int
xml_get_uint(
  xmlDocPtr doc_ptr,
  const char * xpath,
  unsigned int * value_ptr);

int
xml_check_path(
  xmlDocPtr doc_ptr,
  const char * xpath);

#endif /* #ifndef XML_H__FB295C79_9892_4896_9C01_F5EE8F0A2482__INCLUDED */
