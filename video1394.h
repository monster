/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 *   Stuff required to interface video1394 kernel module
 *
 *   Copyright (C) 2006,2007 Nedko Arnaudov <nedko@arnaudov.name>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; version 2 of the License
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *****************************************************************************/

#ifndef VIDEO1394_H__A8E0419B_5546_43DA_8417_3DBE08C8DF03__INCLUDED
#define VIDEO1394_H__A8E0419B_5546_43DA_8417_3DBE08C8DF03__INCLUDED

#define VIDEO1394_MAX_SIZE 0x4000000

#define VIDEO1394_BUFFER_FREE     0
#define VIDEO1394_BUFFER_QUEUED   1
#define VIDEO1394_BUFFER_READY    2

#define VIDEO1394_SYNC_FRAMES          0x00000001
#define VIDEO1394_INCLUDE_ISO_HEADERS  0x00000002
#define VIDEO1394_VARIABLE_PACKET_SIZE 0x00000004

struct video1394_mmap
{
  int channel;                  /* -1 to find an open channel in LISTEN/TALK */
  unsigned int sync_tag;
  unsigned int nb_buffers;
  unsigned int buf_size;
  unsigned int packet_size;     /* For VARIABLE_PACKET_SIZE: Maximum packet size */
  unsigned int fps;
  unsigned int syt_offset;
  unsigned int flags;
};

/* For TALK_QUEUE_BUFFER with VIDEO1394_VARIABLE_PACKET_SIZE use */
struct video1394_queue_variable
{
  unsigned int channel;
  unsigned int buffer;
  unsigned int * packet_sizes;  /* Buffer of size: buf_size / packet_size  */
};

struct video1394_wait
{
  unsigned int channel;
  unsigned int buffer;
  struct timeval filltime;      /* time of buffer full */
};

#define VIDEO1394_LISTEN_CHANNEL                \
  _IOWR('#', 0x10, struct video1394_mmap)
#define VIDEO1394_UNLISTEN_CHANNEL              \
  _IOW ('#', 0x11, int)
#define VIDEO1394_LISTEN_QUEUE_BUFFER           \
  _IOW ('#', 0x12, struct video1394_wait)
#define VIDEO1394_LISTEN_WAIT_BUFFER            \
  _IOWR('#', 0x13, struct video1394_wait)
#define VIDEO1394_TALK_CHANNEL                  \
  _IOWR('#', 0x14, struct video1394_mmap)
#define VIDEO1394_UNTALK_CHANNEL                \
  _IOW ('#', 0x15, int)
#define VIDEO1394_TALK_QUEUE_BUFFER             \
  _IOW ('#', 0x16, size_t)
#define VIDEO1394_TALK_WAIT_BUFFER              \
  _IOW ('#', 0x17, struct video1394_wait)
#define VIDEO1394_LISTEN_POLL_BUFFER            \
  _IOWR('#', 0x18, struct video1394_wait)

#define TARGETBUFSIZE       320
#define MAX_PACKET_SIZE     512
#define VBUF_SIZE           (320*512)

#endif /* #ifndef VIDEO1394_H__A8E0419B_5546_43DA_8417_3DBE08C8DF03__INCLUDED */
