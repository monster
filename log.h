/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 *   This file is part of monster
 *
 *   Copyright (C) 2006,2007 Nedko Arnaudov <nedko@arnaudov.name>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; version 2 of the License
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *****************************************************************************/

#ifndef LOG_H__84A7D7A0_6E97_4044_B56F_92F9060F1C29__INCLUDED
#define LOG_H__84A7D7A0_6E97_4044_B56F_92F9060F1C29__INCLUDED

void logwrite(int lvl, const char * format, ...);

#if defined(DISABLE_ALL_OUTPUT)
# define OUTPUT_MESSAGE(format, arg...)
#else
# define OUTPUT_MESSAGE(format, arg...) logwrite(0, format, ## arg)
#endif

#if defined(DISABLE_DEBUG_OUTPUT)
# define DEBUG_OUT(format, arg...)
#else
# define DEBUG_OUT(format, arg...) logwrite(2, format, ## arg)
#endif

#define ERROR_OUT(format, arg...) logwrite(-1, format, ## arg)

#if defined(DISABLE_NOTICE_OUTPUT)
# define NOTICE_OUT(format, arg...)
#else
# define NOTICE_OUT(format, arg...) logwrite(1, format, ## arg)
#endif

#endif /* #ifndef LOG_H__84A7D7A0_6E97_4044_B56F_92F9060F1C29__INCLUDED */
