/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 *   This file is part of monster
 *
 *   Copyright (C) 2006,2007 Nedko Arnaudov <nedko@arnaudov.name>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; version 2 of the License
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *****************************************************************************/

#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>

#include "log.h"

#define FORMATTING_BUFFER_SIZE   4096

void logwrite_file(const char * message)
{
  FILE * f;

  f = fopen("/var/log/monster.log", "a");
  if (f == NULL)
  {
    /* shit happens */
    /* we need cr because we use curses library */
    fprintf(stderr, "\rfailed to open/create /var/log/monster.log. error %d (%s)\r\n", errno, strerror(errno));
    return;
  }

  fwrite(message, strlen(message), 1, f);
  fwrite("\n", 1, 1, f);

  fclose(f);
}

void logwrite_stderr(const char * message)
{
  fwrite("\r", 1, 1, stderr);
  fwrite(message, strlen(message), 1, stderr);
  fwrite("\r\n", 2, 1, stderr); /* we need cr because we use curses library */
}

void logwrite_va(int lvl, const char * format, va_list arglist)
{
  char buffer[FORMATTING_BUFFER_SIZE];

  vsnprintf(buffer, FORMATTING_BUFFER_SIZE-1, format, arglist);

  if (lvl <= 0)
  {
    logwrite_stderr(buffer);
  }

  logwrite_file(buffer);
}

void logwrite(int lvl, const char * format, ...)
{
  va_list arglist;

  va_start(arglist, format);
  logwrite_va(lvl, format, arglist);
  va_end(arglist);
}
