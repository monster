/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 *   XML parsing helpers
 *
 *   Copyright (C) 2006,2007 Nedko Arnaudov <nedko@arnaudov.name>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; version 2 of the License
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *****************************************************************************/

#include <libxml/parser.h>
#include <libxml/xpath.h>
#include <string.h>

#define DISABLE_DEBUG_OUTPUT

#include "xml.h"
#include "log.h"

int
xml_get_content(
  xmlDocPtr doc_ptr,
  const char * xpath,
  xmlBufferPtr content_buffer_ptr)
{
  int ret;
  xmlXPathContextPtr xpath_ctx;
  xmlXPathObjectPtr xpath_obj;

  /* Create xpath evaluation context */
  xpath_ctx = xmlXPathNewContext(doc_ptr);
  if (xpath_ctx == NULL)
  {
    ERROR_OUT("Unable to create new XPath context");
    ret = -1;
    goto exit;
  }

  /* Evaluate xpath expression */
  xpath_obj = xmlXPathEvalExpression((const xmlChar *)xpath, xpath_ctx);
  if (xpath_obj == NULL)
  {
    ERROR_OUT("Unable to evaluate XPath expression \"%s\"", xpath);
    ret = -1;
    goto free_xpath_ctx;
  }

  if (xpath_obj->nodesetval == NULL || xpath_obj->nodesetval->nodeNr == 0)
  {
    ERROR_OUT("XPath \"%s\" evaluation returned no data", xpath);
    ret = -1;
    goto free_xpath_obj;
  }

  if (xpath_obj->nodesetval->nodeNr != 1)
  {
    ERROR_OUT("XPath \"%s\" evaluation returned multiple entries (%d)", xpath, xpath_obj->nodesetval->nodeNr);
    ret = -1;
    goto free_xpath_obj;
  }

  ret = xmlNodeBufGetContent(content_buffer_ptr, xpath_obj->nodesetval->nodeTab[0]);
  if (ret != 0)
  {
    ERROR_OUT("xmlNodeBufGetContent() failed. (%d)", ret);
    ret = -1;
    goto free_xpath_obj;
  }

  ret = 0;

free_xpath_obj:
  xmlXPathFreeObject(xpath_obj);

free_xpath_ctx:
  xmlXPathFreeContext(xpath_ctx);

exit:
  return ret;
}

int
xml_check_path(
  xmlDocPtr doc_ptr,
  const char * xpath)
{
  int ret;
  xmlXPathContextPtr xpath_ctx;
  xmlXPathObjectPtr xpath_obj;

  /* Create xpath evaluation context */
  xpath_ctx = xmlXPathNewContext(doc_ptr);
  if (xpath_ctx == NULL)
  {
    ERROR_OUT("Unable to create new XPath context");
    ret = -1;
    goto exit;
  }

  /* Evaluate xpath expression */
  xpath_obj = xmlXPathEvalExpression((const xmlChar *)xpath, xpath_ctx);
  if (xpath_obj == NULL)
  {
    ERROR_OUT("Unable to evaluate XPath expression \"%s\"", xpath);
    ret = -1;
    goto free_xpath_ctx;
  }

  if (xpath_obj->nodesetval == NULL || xpath_obj->nodesetval->nodeNr == 0)
  {
    DEBUG_OUT("XPath \"%s\" evaluation returned no data", xpath);
    ret = 0;
    goto free_xpath_obj;
  }

  if (xpath_obj->nodesetval->nodeNr != 1)
  {
    DEBUG_OUT("XPath \"%s\" evaluation returned multiple entries (%d)", xpath, xpath_obj->nodesetval->nodeNr);
    ret = 2;
    goto free_xpath_obj;
  }

  DEBUG_OUT("XPath \"%s\" evaluation returned one entry", xpath);
  ret = 1;

free_xpath_obj:
  xmlXPathFreeObject(xpath_obj);

free_xpath_ctx:
  xmlXPathFreeContext(xpath_ctx);

exit:
  return ret;
}

int
xml_get_string(
  xmlDocPtr doc_ptr,
  const char * xpath,
  char * buffer_ptr,
  size_t buffer_size)
{
  int ret;
  xmlBufferPtr content_buffer_ptr;

  content_buffer_ptr = xmlBufferCreate();
  if (content_buffer_ptr == NULL)
  {
    ERROR_OUT("xmlBufferCreate() failed.");
    ret = -1;
    goto exit;
  }

  ret = xml_get_content(doc_ptr, xpath, content_buffer_ptr);
  if (ret != 0)
  {
    goto free_buffer;
  }

  DEBUG_OUT(
    "XPath \"%s\" execution returned content \"%s\" (%u bytes)",
    xpath,
    xmlBufferContent(content_buffer_ptr),
    xmlBufferLength(content_buffer_ptr));

  if (buffer_size < xmlBufferLength(content_buffer_ptr) + 1)
  {
    ERROR_OUT(
      "%u chars are requires to store \"%s\" but only %u are available",
      xmlBufferLength(content_buffer_ptr) + 1,
      xmlBufferContent(content_buffer_ptr),
      buffer_size);
    ret = -1;
    goto free_buffer;
  }

  memcpy(buffer_ptr, xmlBufferContent(content_buffer_ptr), xmlBufferLength(content_buffer_ptr) + 1);
  ret = 0;

free_buffer:
  xmlBufferFree(content_buffer_ptr);

exit:
  return ret;
}

int
xml_get_string_dup(
  xmlDocPtr doc_ptr,
  const char * xpath,
  char ** buffer_ptr_ptr)
{
  int ret;
  xmlBufferPtr content_buffer_ptr;
  char * buffer_ptr;

  content_buffer_ptr = xmlBufferCreate();
  if (content_buffer_ptr == NULL)
  {
    ERROR_OUT("xmlBufferCreate() failed.");
    ret = -1;
    goto exit;
  }

  ret = xml_get_content(doc_ptr, xpath, content_buffer_ptr);
  if (ret != 0)
  {
    goto free_buffer;
  }

  DEBUG_OUT(
    "XPath \"%s\" execution returned content \"%s\" (%u bytes)",
    xpath,
    xmlBufferContent(content_buffer_ptr),
    xmlBufferLength(content_buffer_ptr));

  buffer_ptr = strdup((const char *)xmlBufferContent(content_buffer_ptr));
  if (buffer_ptr == NULL)
  {
    ERROR_OUT("strdup failed.");
    ret = -1;
    goto free_buffer;
  }

  *buffer_ptr_ptr = buffer_ptr;
  ret = 0;

free_buffer:
  xmlBufferFree(content_buffer_ptr);

exit:
  return ret;
}

int
xml_get_int(
  xmlDocPtr doc_ptr,
  const char * xpath,
  int * value_ptr)
{
  int ret;
  xmlBufferPtr content_buffer_ptr;

  content_buffer_ptr = xmlBufferCreate();
  if (content_buffer_ptr == NULL)
  {
    ERROR_OUT("xmlBufferCreate() failed.");
    ret = -1;
    goto exit;
  }

  ret = xml_get_content(doc_ptr, xpath, content_buffer_ptr);
  if (ret != 0)
  {
    goto free_buffer;
  }

  DEBUG_OUT(
    "XPath \"%s\" execution returned content \"%s\" (%u bytes)",
    xpath,
    xmlBufferContent(content_buffer_ptr),
    xmlBufferLength(content_buffer_ptr));

  *value_ptr = atoi((const char *)xmlBufferContent(content_buffer_ptr));
  ret = 0;

free_buffer:
  xmlBufferFree(content_buffer_ptr);

exit:
  return ret;
}

int
xml_get_uint(
  xmlDocPtr doc_ptr,
  const char * xpath,
  unsigned int * value_ptr)
{
  int ret;
  xmlBufferPtr content_buffer_ptr;
  int value;

  content_buffer_ptr = xmlBufferCreate();
  if (content_buffer_ptr == NULL)
  {
    ERROR_OUT("xmlBufferCreate() failed.");
    ret = -1;
    goto exit;
  }

  ret = xml_get_content(doc_ptr, xpath, content_buffer_ptr);
  if (ret != 0)
  {
    goto free_buffer;
  }

  DEBUG_OUT(
    "XPath \"%s\" execution returned content \"%s\" (%u bytes)",
    xpath,
    xmlBufferContent(content_buffer_ptr),
    xmlBufferLength(content_buffer_ptr));

  value = atoi((const char *)xmlBufferContent(content_buffer_ptr));
  if (value < 0)
  {
    ERROR_OUT("\"%s\" cannot be converted to unsigned int", xmlBufferContent(content_buffer_ptr));
    return -1;
  }

  *value_ptr = (unsigned int)value;
  ret = 0;

free_buffer:
  xmlBufferFree(content_buffer_ptr);

exit:
  return ret;
}
