/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 *   player interface
 *   This file is part of monster
 *
 *   Copyright (C) 2006,2007 Nedko Arnaudov <nedko@arnaudov.name>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; version 2 of the License
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *****************************************************************************/

#ifndef PLAYER_H__CC9B87C2_8B1D_47F2_96B5_CF107C204E08__INCLUDED
#define PLAYER_H__CC9B87C2_8B1D_47F2_96B5_CF107C204E08__INCLUDED

struct player;

typedef void (* player_destroy_t)(struct player * player_ptr);

typedef int (* player_start_prepare_t)(struct player * player_ptr);

typedef int (* player_start_t)(struct player * player_ptr);

struct player
{
  player_destroy_t destroy;
  player_start_prepare_t start_prepare;
  player_start_t start;
  char name[1024];
};

static __inline__
void
player_destroy(struct player * player_ptr)
{
  return player_ptr->destroy(player_ptr);
};

static __inline__
int
player_start_prepare(struct player * player_ptr)
{
  return player_ptr->start_prepare(player_ptr);
};

static __inline__
int
player_start(struct player * player_ptr)
{
  return player_ptr->start(player_ptr);
};

/* end of data callback */
typedef void (* eod_t)(unsigned int context);

#endif /* #ifndef PLAYER_H__CC9B87C2_8B1D_47F2_96B5_CF107C204E08__INCLUDED */
