# This file is part of moster
#
# Copyright (C) 2006,2007 Nedko Arnaudov <nedko@arnaudov.name>
#  
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.

default: monster

GENDEP_SED_EXPR = "s/^\\(.*\\)\\.o *: /$(subst /,\/,$(@:.d=.o)) $(subst /,\/,$@) : /g"
GENDEP_C = set -e; $(GCC_PREFIX)gcc -MM $(CFLAGS) $< | sed $(GENDEP_SED_EXPR) > $@; [ -s $@ ] || rm -f $@

CFLAGS = -g -Wall -Werror -Wcast-align -Wno-strict-aliasing -Werror-implicit-function-declaration -D_FILE_OFFSET_BITS=64 -D_GNU_SOURCE
LIBS = -lpthread -lasound -lcurses

CFLAGS += $(shell pkg-config --cflags libxml-2.0)
LIBS += $(shell pkg-config --libs libxml-2.0)

SOURCES = player_video1394.c player_alsa_pcm.c player_alsa_midi.c media_source_dv.c media_source_wav.c media_source_midi.c monster.c conf.c xml.c song_player.c playback.c log.c path.c

OBJECTS = $(SOURCES:%.c=%.o)
DEP_FILES = $(OBJECTS:.o=.d)

monster: $(OBJECTS)
	@echo "Linking $@ ..."
	@gcc $(OBJECTS) -o monster $(LIBS)

clean:
	-rm monster $(OBJECTS)

fullclean: clean
	-rm $(DEP_FILES)

%.o:%.c
	@echo "Compiling $< to $@ ..."
	@gcc -c $(CFLAGS) $< -o $@

%.d:%.c
	@echo "Creating dependency file $@ for $<"
	@$(GENDEP_C)

# All object and dependency files depend on this file
$(OBJECTS) $(DEP_FILES): GNUmakefile

-include $(DEP_FILES)
