/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 *   Play one song and be able to stop playback
 *   This file is part of monster
 *
 *   Copyright (C) 2006,2007 Nedko Arnaudov <nedko@arnaudov.name>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; version 2 of the License
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *****************************************************************************/

#ifndef SONG_PLAYER_H__9132D3B7_5ED7_425C_8E8D_3B57CEE87A4A__INCLUDED
#define SONG_PLAYER_H__9132D3B7_5ED7_425C_8E8D_3B57CEE87A4A__INCLUDED

/* end of song callback */
typedef void (* eos_t)();

int
play_song(
  const char * song_filename_ptr,
  eos_t eos);

void
end_song();

int
get_song_name(
  const char * song_filename_ptr,
  char ** song_name_ptr_ptr);

#endif /* #ifndef SONG_PLAYER_H__9132D3B7_5ED7_425C_8E8D_3B57CEE87A4A__INCLUDED */
