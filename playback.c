/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 *   Playback control and playlist handling
 *   This file is part of monster
 *
 *   Copyright (C) 2006,2007 Nedko Arnaudov <nedko@arnaudov.name>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; version 2 of the License
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *****************************************************************************/

#include <stdlib.h>
#include <pthread.h>
#include <libxml/parser.h>
#include <libxml/xpath.h>
#include <string.h>
#include <libgen.h>
#include <unistd.h>

#define DISABLE_DEBUG_OUTPUT

#include "playback.h"
#include "song_player.h"
#include "log.h"
#include "song_player.h"
#include "types.h"
#include "list.h"
#include "xml.h"
#include "path.h"

/* XPath expression used for song path selection */
#define XPATH_SONG_PATH_EXPRESSION "/playlist/song/path"

pthread_mutex_t g_playback_mutex;
pthread_cond_t g_playback_cond;
pthread_t g_playback_thread;
bool g_destroy_flag;
bool g_playback_toggle_flag;
bool g_eos_flag;
signed int g_roll;
unsigned int g_goto;

struct playlist_entry
{
  struct list_head siblings;
  char * song_name_ptr;
  char * song_filename_ptr;
  int intro_silence;
};

struct list_head g_playlist;

struct playlist_entry * g_current_song_ptr;

void
playlist_free()
{
  struct list_head * node_ptr;
  struct playlist_entry * entry_ptr;

  DEBUG_OUT("playlist_free() called.");

  while (!list_empty(&g_playlist))
  {
    node_ptr = g_playlist.next;

    list_del(node_ptr);

    entry_ptr = list_entry(node_ptr, struct playlist_entry, siblings);

    free(entry_ptr->song_name_ptr);

    free(entry_ptr->song_filename_ptr);

    free(entry_ptr);
  }
}

void
playlist_display()
{
  struct list_head * node_ptr;
  struct playlist_entry * entry_ptr;
  int i;
  
  OUTPUT_MESSAGE("-------- Playlist ---------");

  i = 0;

  list_for_each(node_ptr, &g_playlist)
  {
    entry_ptr = list_entry(node_ptr, struct playlist_entry, siblings);

    if (entry_ptr->intro_silence != 0)
    {
      OUTPUT_MESSAGE("%02i - %s (%d seconds intro silence)", i+1, entry_ptr->song_name_ptr, entry_ptr->intro_silence);
    }
    else
    {
      OUTPUT_MESSAGE("%02i - %s", i+1, entry_ptr->song_name_ptr);
    }

    i++;
  }

  OUTPUT_MESSAGE("------------------");
}

int
playlist_build(const char * playlist_filename_ptr)
{
  int ret;
  xmlDocPtr doc_ptr;
  xmlXPathContextPtr xpath_ctx_ptr;
  xmlXPathObjectPtr xpath_obj_ptr;
  xmlXPathObjectPtr xpath_obj2_ptr;
  xmlBufferPtr content_buffer_ptr;
  int i;
  char * playlist_name_ptr;
  char * playlist_filename_copy_ptr;
  const char * playlist_dirname_ptr;
  size_t playlist_dirname_size;
  struct playlist_entry * entry_ptr;
  const char * song_filename_ptr;
  size_t temp_size;
  int intro_silence;
  char xpath[1000];

  DEBUG_OUT("Loading playlist from \"%s\"", playlist_filename_ptr);

  INIT_LIST_HEAD(&g_playlist);

  playlist_filename_copy_ptr = strdup(playlist_filename_ptr);
  if (playlist_filename_copy_ptr == NULL)
  {
    ERROR_OUT("strdup() failed");
    ret = -1;
    goto exit;
  }

  playlist_dirname_ptr = dirname(playlist_filename_copy_ptr);

  DEBUG_OUT("playlist dirname is \"%s\"", playlist_dirname_ptr);

  playlist_dirname_size = strlen(playlist_dirname_ptr);

  doc_ptr = xmlParseFile(playlist_filename_ptr);
  if (doc_ptr == NULL)
  {
    ERROR_OUT("Failed to parse playlist \"%s\"", playlist_filename_ptr);
    ret = -1;
    goto free_playlist_filename_copy;
  }

  /* Create xpath evaluation context */
  xpath_ctx_ptr = xmlXPathNewContext(doc_ptr);
  if (xpath_ctx_ptr == NULL)
  {
    ERROR_OUT("Unable to create new XPath context");
    ret = -1;
    goto free_doc;
  }

  /* Evaluate xpath expression */
  xpath_obj_ptr = xmlXPathEvalExpression((const xmlChar *)XPATH_SONG_PATH_EXPRESSION, xpath_ctx_ptr);
  if (xpath_obj_ptr == NULL)
  {
    ERROR_OUT("Unable to evaluate XPath expression \"%s\"", XPATH_SONG_PATH_EXPRESSION);
    ret = -1;
    goto free_xpath_ctx;
  }

  if (xpath_obj_ptr->nodesetval == NULL || xpath_obj_ptr->nodesetval->nodeNr == 0)
  {
    ERROR_OUT("XPath \"%s\" evaluation returned no data", XPATH_SONG_PATH_EXPRESSION);
    ret = -1;
    goto free_xpath_obj;
  }

  content_buffer_ptr = xmlBufferCreate();
  if (content_buffer_ptr == NULL)
  {
    ERROR_OUT("xmlBufferCreate() failed.");
    ret = -1;
    goto free_xpath_obj;
  }

  ret = xml_get_string_dup(doc_ptr, "/playlist/name", &playlist_name_ptr);
  if (ret != 0)
  {
    ERROR_OUT("xml_get_string_dup() failed. Cannot get playlist name (%d)", ret);
    ret = -1;
    goto free_buffer;
  }

  OUTPUT_MESSAGE("-------- Playlist \"%s\" ---------", playlist_name_ptr);

  for (i = 0 ; i < xpath_obj_ptr->nodesetval->nodeNr ; i++)
  {
    //DEBUG_OUT("song at index %d", i);

    {
      intro_silence = 0;

      sprintf(xpath, "/playlist/song[%d]/intro_silence", i + 1);

      /* Evaluate xpath expression */
      xpath_obj2_ptr = xmlXPathEvalExpression((const xmlChar *)xpath, xpath_ctx_ptr);
      if (xpath_obj2_ptr == NULL)
      {
        ERROR_OUT("Unable to evaluate xpath expression for selecting intro_silence");
      }
      else
      {
        if (xpath_obj2_ptr->nodesetval == NULL || xpath_obj2_ptr->nodesetval->nodeNr == 0)
        {
          DEBUG_OUT("XPath evaluation returned no data (%s)", xpath);
        }
        else if (xpath_obj2_ptr->nodesetval->nodeNr != 1)
        {
          ERROR_OUT("XPath evaluation returned too many data");
        }
        else
        {
          ret = xmlNodeBufGetContent(content_buffer_ptr, xpath_obj2_ptr->nodesetval->nodeTab[0]);
          if (ret != 0)
          {
            ERROR_OUT("xmlNodeBufGetContent() failed. (%d)", ret);
          }
          else
          {
            DEBUG_OUT("Intro silence is %s seconds", (const char *)xmlBufferContent(content_buffer_ptr));
            intro_silence = atoi((const char *)xmlBufferContent(content_buffer_ptr));
            if (intro_silence < 0)
            {
              intro_silence = 0;
            }
          }

          xmlBufferEmpty(content_buffer_ptr);
        }
      }
    }

    ret = xmlNodeBufGetContent(content_buffer_ptr, xpath_obj_ptr->nodesetval->nodeTab[i]);
    if (ret != 0)
    {
      ERROR_OUT("xmlNodeBufGetContent() failed. (%d)", ret);
      ret = -1;
      goto free_playlist;
    }

    entry_ptr = (struct playlist_entry *)malloc(sizeof(struct playlist_entry));
    if (entry_ptr == NULL)
    {
      ERROR_OUT("malloc() failed.");
      ret = -1;
      goto free_playlist;
    }

    entry_ptr->intro_silence = intro_silence;

    song_filename_ptr = (const char *)xmlBufferContent(content_buffer_ptr);
    DEBUG_OUT("Song filename \"%s\"", song_filename_ptr);
    temp_size = xmlBufferLength(content_buffer_ptr);

    entry_ptr->song_filename_ptr = (char *)malloc(playlist_dirname_size + 1 + temp_size + 1);
    if (entry_ptr->song_filename_ptr == NULL)
    {
      ERROR_OUT("malloc() failed.");
      ret = -1;
      goto free_entry;
    }

    memcpy(entry_ptr->song_filename_ptr, playlist_dirname_ptr, playlist_dirname_size);
    entry_ptr->song_filename_ptr[playlist_dirname_size] = '/';
    memcpy(entry_ptr->song_filename_ptr + playlist_dirname_size + 1, song_filename_ptr, temp_size);
    entry_ptr->song_filename_ptr[playlist_dirname_size + 1 + temp_size] = 0;

    xmlBufferEmpty(content_buffer_ptr);

    path_normalize(entry_ptr->song_filename_ptr);

    ret = get_song_name(entry_ptr->song_filename_ptr, &entry_ptr->song_name_ptr);
    if (ret != 0)
    {
      ERROR_OUT("Cannot get song name from \"%s\"", xmlBufferContent(content_buffer_ptr));
      ret = -1;
      goto free_song_filename;
    }

    if (entry_ptr->intro_silence != 0)
    {
      OUTPUT_MESSAGE("%02i - %s (%d seconds intro silence)", i+1, entry_ptr->song_name_ptr, entry_ptr->intro_silence);
    }
    else
    {
      OUTPUT_MESSAGE("%02i - %s", i+1, entry_ptr->song_name_ptr);
    }

    list_add_tail(&entry_ptr->siblings, &g_playlist);
  }

  OUTPUT_MESSAGE("------------------");

  if (list_empty(&g_playlist))
  {
    ERROR_OUT("There is no point to play empty playlist");
    ret = -1;
    goto free_playlist;
  }

  /* Make first song current */
  g_current_song_ptr = list_entry(g_playlist.next, struct playlist_entry, siblings);

  ret = 0;
  goto free_playlist_name;

free_song_filename:
  free(entry_ptr->song_filename_ptr);

free_entry:
  free(entry_ptr);

free_playlist:
  playlist_free();

free_playlist_name:
  free(playlist_name_ptr);

free_buffer:
  xmlBufferFree(content_buffer_ptr);

free_xpath_obj:
  xmlXPathFreeObject(xpath_obj_ptr);

free_xpath_ctx:
  xmlXPathFreeContext(xpath_ctx_ptr);

free_doc:
  xmlFreeDoc(doc_ptr);

free_playlist_filename_copy:
  free(playlist_filename_copy_ptr);
exit:
  return ret;
}

void
end_of_song_callback()
{
  NOTICE_OUT("End of song");
  pthread_mutex_lock(&g_playback_mutex);
  g_eos_flag = true;
  pthread_cond_signal(&g_playback_cond);
  pthread_mutex_unlock(&g_playback_mutex);
}

void * playback_thread(void * context)
{
  int ret;
  bool playback_toggle_flag = !g_playback_toggle_flag;
  bool playing_flag = false;
  bool song_changed;
  struct list_head * node_ptr;
  unsigned int index;
  bool acted;
  int silence_counter;

  pthread_mutex_lock(&g_playback_mutex);
loop:
  acted = false;

  if (g_destroy_flag)
  {
    pthread_mutex_unlock(&g_playback_mutex);
    goto exit;
  }

  if (g_eos_flag)
  {
    g_eos_flag = false;
    pthread_mutex_unlock(&g_playback_mutex);
    DEBUG_OUT("End of song detected.");
    end_song();
    playing_flag = false;
    pthread_mutex_lock(&g_playback_mutex);
    acted = true;

    if (g_current_song_ptr->siblings.next != &g_playlist)
    {
      OUTPUT_MESSAGE("End of song, moving to next in playlist.");
      g_current_song_ptr = list_entry(g_current_song_ptr->siblings.next, struct playlist_entry, siblings);
      goto play;
    }
    else
    {
      OUTPUT_MESSAGE("End of last song.");
    }
  }

  song_changed = false;

  if (g_goto > 0)
  {
    index = 0;

    list_for_each(node_ptr, &g_playlist)
    {
      index++;

      if (index == g_goto)
      {
        g_current_song_ptr = list_entry(node_ptr, struct playlist_entry, siblings);
        song_changed = true;
        break;
      }
    }

    if (!song_changed)
    {
      ERROR_OUT("there is no song %u (ignoring goto)", g_goto);
    }

    g_goto = 0;
  }

  if (g_roll > 0)
  {
    while (g_roll > 0 && g_current_song_ptr->siblings.next != &g_playlist)
    {
      g_current_song_ptr = list_entry(g_current_song_ptr->siblings.next, struct playlist_entry, siblings);
      g_roll--;
      song_changed = true;
    }

    g_roll = 0;
  }
  else if (g_roll < 0)
  {
    while (g_roll < 0 && g_current_song_ptr->siblings.prev != &g_playlist)
    {
      g_current_song_ptr = list_entry(g_current_song_ptr->siblings.prev, struct playlist_entry, siblings);
      g_roll++;
      song_changed = true;
    }

    g_roll = 0;
  }

  if (song_changed)
  {
    OUTPUT_MESSAGE("Current song changed to \"%s\"", g_current_song_ptr->song_name_ptr);

    if (playing_flag)
    {
      pthread_mutex_unlock(&g_playback_mutex);
      DEBUG_OUT("Stopping because current song changed.");
      end_song();
      playing_flag = false;
      playback_toggle_flag = !g_playback_toggle_flag; /* schedule play after song change */
      pthread_mutex_lock(&g_playback_mutex);
      acted = true;
    }
  }

  if (g_playback_toggle_flag != playback_toggle_flag)
  {
    DEBUG_OUT("Playback toggle detected.");
  play:
    acted = true;
    playback_toggle_flag = g_playback_toggle_flag;
    pthread_mutex_unlock(&g_playback_mutex);

    if (playing_flag)
    {
      DEBUG_OUT("Ending playback.");
      end_song();
      playing_flag = false;
    }
    else
    {
      if (g_current_song_ptr->intro_silence != 0)
      {
        OUTPUT_MESSAGE("Intro silence %d seconds...", g_current_song_ptr->intro_silence);
        silence_counter = g_current_song_ptr->intro_silence;
        while (silence_counter != 0)
        {
          sleep(1);
          if (g_playback_toggle_flag != playback_toggle_flag ||
              g_goto != 0 ||
              g_roll != 0 ||
              g_destroy_flag)
          {
            OUTPUT_MESSAGE("Canceling intro silence.");
            pthread_mutex_lock(&g_playback_mutex);
            goto loop;
          }
          silence_counter--;
          DEBUG_OUT("Intro silence: tick");
        }
      }

      DEBUG_OUT("Starting playback of \"%s\"", g_current_song_ptr->song_name_ptr);
      ret = play_song(g_current_song_ptr->song_filename_ptr, end_of_song_callback);
      if (ret != 0)
      {
        ERROR_OUT("failed to play song");
      }
      else
      {
        playing_flag = true;
      }
    }

    pthread_mutex_lock(&g_playback_mutex);
  }

  if (!acted)
  {
    pthread_cond_wait(&g_playback_cond, &g_playback_mutex);
  }

  goto loop;

exit:
  if (playing_flag)
  {
    DEBUG_OUT("Ending playback.");
    end_song();
  }

  return NULL;
}

int
playback_init(const char * playlist_filename_ptr)
{
  int ret, ret1;

  /* parse and build playlist */
  ret = playlist_build(playlist_filename_ptr);
  if (ret != 0)
  {
    ERROR_OUT("Failed to build playlist");
    ret = -1;
    goto exit;
  }

  /* start playback thread */
  ret = pthread_mutex_init(&g_playback_mutex, NULL);
  if (ret != 0)
  {
    ERROR_OUT("Failed to init real mask mutex (%d)", ret);
    ret = -1;
    goto exit_playlist_cleanup;
  }

  ret = pthread_cond_init(&g_playback_cond, NULL);
  if (ret != 0)
  {
    ERROR_OUT("Failed to init real mask cond (%d)", ret);
    ret = -1;
    goto exit_destroy_mutex;
  }

  ret = pthread_create(&g_playback_thread, NULL, playback_thread, NULL);
  if (ret != 0)
  {
    ERROR_OUT("Failed to start feed thread (%d)", ret);
    ret = -1;
    goto exit_destroy_cond;
  }

  ret = 0;
  goto exit;

exit_destroy_cond:
  ret1 = pthread_cond_destroy(&g_playback_cond);
  if (ret1 != 0)
  {
    ERROR_OUT("Failed to destroy playback cond (%d)", ret1);
  }

exit_destroy_mutex:
  ret1 = pthread_mutex_destroy(&g_playback_mutex);
  if (ret1 != 0)
  {
    ERROR_OUT("Failed to destroy playback mutex (%d)", ret1);
  }

exit_playlist_cleanup:
  playlist_free();

exit:
  return ret;
}

void
playback_toggle()
{
  pthread_mutex_lock(&g_playback_mutex);
  g_playback_toggle_flag = !g_playback_toggle_flag;
  pthread_cond_signal(&g_playback_cond);
  pthread_mutex_unlock(&g_playback_mutex);
}

void
playback_next()
{
  pthread_mutex_lock(&g_playback_mutex);
  g_roll++;
  pthread_cond_signal(&g_playback_cond);
  pthread_mutex_unlock(&g_playback_mutex);
}

void
playback_prev()
{
  pthread_mutex_lock(&g_playback_mutex);
  g_roll--;
  pthread_cond_signal(&g_playback_cond);
  pthread_mutex_unlock(&g_playback_mutex);
}

void
playback_goto(unsigned int song)
{
  pthread_mutex_lock(&g_playback_mutex);
  g_roll = 0;
  g_goto = song;
  pthread_cond_signal(&g_playback_cond);
  pthread_mutex_unlock(&g_playback_mutex);
}

void
playback_destroy()
{
  pthread_mutex_lock(&g_playback_mutex);
  g_destroy_flag = true;
  pthread_cond_signal(&g_playback_cond);
  pthread_mutex_unlock(&g_playback_mutex);

  pthread_join(g_playback_thread, NULL);

  pthread_cond_destroy(&g_playback_cond);
  pthread_mutex_destroy(&g_playback_mutex);

  playlist_free();
}
