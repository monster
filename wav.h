/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 *   Definitions for Microsoft WAVE format
 *
 *   Copyright (C) 2006,2007 Nedko Arnaudov <nedko@arnaudov.name>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; version 2 of the License
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *****************************************************************************/

#ifndef WAV_H__C70F6E68_1B32_4230_A6BD_F32948477BDD__INCLUDED
#define WAV_H__C70F6E68_1B32_4230_A6BD_F32948477BDD__INCLUDED

#if __BYTE_ORDER == __LITTLE_ENDIAN
#define COMPOSE_ID(a,b,c,d) ((a) | ((b)<<8) | ((c)<<16) | ((d)<<24))
#define LE_16(v)   (v)
#define LE_32(v)   (v)
#define BE_16(v)   bswap_16(v)
#define BE_32(v)   bswap_32(v)
#elif __BYTE_ORDER == __BIG_ENDIAN
#define COMPOSE_ID(a,b,c,d) ((d) | ((c)<<8) | ((b)<<16) | ((a)<<24))
#define LE_16(v)   bswap_16(v)
#define LE_32(v)   bswap_32(v)
#define BE_16(v)   (v)
#define BE_32(v)   (v)
#else
#error "Unsupported/unknown endian"
#endif

#define WAV_RIFF    COMPOSE_ID('R','I','F','F')
#define WAV_WAVE    COMPOSE_ID('W','A','V','E')
#define WAV_FMT     COMPOSE_ID('f','m','t',' ')
#define WAV_DATA    COMPOSE_ID('d','a','t','a')
#define WAV_PCM_CODE    1

/* it's in chunks like .voc and AMIGA iff, but my source say there
   are in only in this combination, so I combined them in one header;
   it works on all WAVE-file I have
*/
struct wave_header
{
  uint32 magic;                 /* 'RIFF' */
  uint32 length;                /* filelen */
  uint32 type;                  /* 'WAVE' */
} __attribute__((packed));

struct wave_fmt_body
{
  uint16 format;                /* should be 1 for PCM-code */
  uint16 modus;                 /* 1 Mono, 2 Stereo */
  uint32 sample_fq;             /* frequence of sample */
  uint32 byte_p_sec;
  uint16 byte_p_spl;            /* samplesize; 1 or 2 bytes */
  uint16 bit_p_spl;             /* 8, 12 or 16 bit */
} __attribute__((packed));

struct wave_chunk_header
{
  uint32 type;                  /* 'data' */
  uint32 length;                /* samplecount */
} __attribute__((packed));

/* canonical header */
struct wave_cannonical_header
{
  struct wave_header riff_header;
  struct wave_chunk_header fmt_header;
  struct wave_fmt_body fmt_body;
} __attribute__((packed));

/* these are not directly related to on disk WAV format but we need to place them somewhere */
#define WAV_DATA_CHUNK        4096 /* samples, one channel */
#define WAV_DATA_STEREO_16    (4096 * 2 * 2)

#endif /* #ifndef WAV_H__C70F6E68_1B32_4230_A6BD_F32948477BDD__INCLUDED */
