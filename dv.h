/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 *   This file is part of monster
 *
 *   Copyright (C) 2006,2007 Nedko Arnaudov <nedko@arnaudov.name>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; version 2 of the License
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *****************************************************************************/

#ifndef DV_H__991180EC_D25E_41E7_BAC4_60E404E0F542__INCLUDED
#define DV_H__991180EC_D25E_41E7_BAC4_60E404E0F542__INCLUDED

#define DV_FRAME_SIZE_PAL    144000
#define DV_FRAME_SIZE_NTSC   120000

#endif /* #ifndef DV_H__991180EC_D25E_41E7_BAC4_60E404E0F542__INCLUDED */
