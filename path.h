/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 *   Filesystem path helpers
 *   This file is part of monster
 *
 *   Copyright (C) 2006,2007 Nedko Arnaudov <nedko@arnaudov.name>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; version 2 of the License
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *****************************************************************************/

#ifndef PATH_H__104DC46E_9FBF_480F_A677_893A1B0AD3F4__INCLUDED
#define PATH_H__104DC46E_9FBF_480F_A677_893A1B0AD3F4__INCLUDED

/* consume '../'s and './'s */
void
path_normalize(char * path_ptr);

#endif /* #ifndef PATH_H__104DC46E_9FBF_480F_A677_893A1B0AD3F4__INCLUDED */
