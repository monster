/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 *   Player capable to send MIDI data to ALSA
 *   This file is part of monster
 *
 *   Copyright (C) 2006,2007 Nedko Arnaudov <nedko@arnaudov.name>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; version 2 of the License
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *****************************************************************************/
/*****************************************************************************
 *
 *  We leave ALSA sequencer internal queue mechanism to send events itself.
 *  "softirq-high/0" kernel thread needs to be with high enough priority
 *  because ALSA seems to use it when sending events. Otherwise, MIDI event
 *  timings will get wrong under heavy system load (like IDE IRQ processing)
 *  
 *****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>
#include <alsa/asoundlib.h>
#include <alsa/seq.h>
#include <pthread.h>
#include <signal.h>

#define DISABLE_DEBUG_OUTPUT

#include "media_source.h"
#include "player.h"
#include "player_alsa_midi.h"
#include "log.h"
#include "midi_event.h"
#include "types.h"
#include "conf.h"

#define FEED_THREAD_STATE_INITIAL         0
#define FEED_THREAD_STATE_ARMED           1
#define FEED_THREAD_STATE_PLAY            2
#define FEED_THREAD_STATE_STOP_REQUESTED  3
#define FEED_THREAD_STATE_STOPPED         4

struct player_alsa_midi
{
  struct player virtual;
  struct media_source * media_source_ptr;

  eod_t eod;
  unsigned int eod_context;

  snd_seq_t * seq;
  snd_seq_addr_t port;
  int our_port;
  snd_seq_queue_tempo_t * queue_tempo_ptr;
  int queue;

  unsigned int rt_prio_alsa_midi_feed;

  pthread_t feed_thread;

  pthread_mutex_t feed_mutex;
  pthread_cond_t feed_cond;
  unsigned int feed_thread_state;

  bool eod_source;
};

#define perm_ok(pinfo,bits) ((snd_seq_port_info_get_capability(pinfo) & (bits)) == (bits))

void
player_alsa_midi_print_destination_info(
  struct player_alsa_midi * player_ptr)
{
  snd_seq_client_info_t *cinfo;
  snd_seq_port_info_t *pinfo;

  snd_seq_client_info_alloca(&cinfo);
  snd_seq_port_info_alloca(&pinfo);
  snd_seq_client_info_set_client(cinfo, -1);

  /* Try to find the destination port by searching all ports */
  while (snd_seq_query_next_client(player_ptr->seq, cinfo) >= 0)
  {
    if (player_ptr->port.client == snd_seq_client_info_get_client(cinfo))
    {
      /* reset query info */
      snd_seq_port_info_set_client(pinfo, snd_seq_client_info_get_client(cinfo));
      snd_seq_port_info_set_port(pinfo, -1);

      while (snd_seq_query_next_port(player_ptr->seq, pinfo) >= 0)
      {
        if (player_ptr->port.port == snd_seq_port_info_get_port(pinfo) &&
            perm_ok(pinfo, SND_SEQ_PORT_CAP_WRITE|SND_SEQ_PORT_CAP_SUBS_WRITE))
        {
          if (strcmp(snd_seq_client_info_get_name(cinfo), snd_seq_port_info_get_name(pinfo)) == 0)
          {
            NOTICE_OUT(
              "MIDI: Sending midi to port %d:%d \"%s\" [type=%s]",
              player_ptr->port.client,
              player_ptr->port.port,
              snd_seq_client_info_get_name(cinfo),
              (snd_seq_client_info_get_type(cinfo) == SND_SEQ_USER_CLIENT ? "user" : "kernel"));
          }
          else
          {
            NOTICE_OUT(
              "MIDI: Sending midi to port %d:%d \"%s\" [type=%s] \"%s\"",
              player_ptr->port.client,
              player_ptr->port.port,
              snd_seq_client_info_get_name(cinfo),
              (snd_seq_client_info_get_type(cinfo) == SND_SEQ_USER_CLIENT ? "user" : "kernel"),
              snd_seq_port_info_get_name(pinfo));
          }

          return;
        }
      }
    }
  }

  /* We've not found the port, print port values anyway, but midi play should fail anyway */
  NOTICE_OUT("MIDI: Sending midi to port %d:%d", player_ptr->port.client, player_ptr->port.port);
}

int
player_alsa_midi_fill_event(
  struct player_alsa_midi * player_ptr,
  snd_seq_event_t * seq_event_ptr)
{
  int ret;
  struct midi_event * event_ptr;

  if (player_ptr->eod_source)
  {
    return -1;
  }

  //DEBUG_OUT("calling media_source_get_frame_data()");

  ret = media_source_get_frame_data(player_ptr->media_source_ptr, (void **)&event_ptr);
  if (ret < 0)
  {
    return -1;
  }

  //DEBUG_OUT("media_source_get_frame_data() completed.");

  if (ret == 1)
  {
    /* schedule queue stop at end of song */
    DEBUG_OUT("%8u: Stop event", seq_event_ptr->time.tick);
    snd_seq_ev_set_fixed(seq_event_ptr);
    seq_event_ptr->type = SND_SEQ_EVENT_STOP;
    seq_event_ptr->dest.client = SND_SEQ_CLIENT_SYSTEM;
    seq_event_ptr->dest.port = SND_SEQ_PORT_SYSTEM_TIMER;
    seq_event_ptr->data.queue.queue = player_ptr->queue;
    player_ptr->eod_source = true;
    return 1;
  }

  media_source_next_frame(player_ptr->media_source_ptr);

  /* output the event */
  seq_event_ptr->time.tick = event_ptr->tick;
  seq_event_ptr->dest = player_ptr->port;
  switch (event_ptr->type)
  {
  case MIDI_EVENT_NOTE_OFF:
    seq_event_ptr->type = SND_SEQ_EVENT_NOTEOFF;
    snd_seq_ev_set_fixed(seq_event_ptr);
    seq_event_ptr->data.note.channel = event_ptr->data.note_off.channel;
    seq_event_ptr->data.note.note = event_ptr->data.note_off.note;
    seq_event_ptr->data.note.velocity = event_ptr->data.note_off.velocity;
/*     DEBUG_OUT( */
/*       "%8u: Channel %u: Note %u:%u on", */
/*       event_ptr->tick, */
/*       event_ptr->data.note_off.channel + 1, */
/*       event_ptr->data.note_off.note, */
/*       event_ptr->data.note_off.velocity); */
    break;
  case MIDI_EVENT_NOTE_ON:
    seq_event_ptr->type = SND_SEQ_EVENT_NOTEON;
    snd_seq_ev_set_fixed(seq_event_ptr);
    seq_event_ptr->data.note.channel = event_ptr->data.note_on.channel;
    seq_event_ptr->data.note.note = event_ptr->data.note_on.note;
    seq_event_ptr->data.note.velocity = event_ptr->data.note_on.velocity;
/*     DEBUG_OUT( */
/*       "%8u: Channel %u: Note %u:%u on", */
/*       event_ptr->tick, */
/*       event_ptr->data.note_on.channel + 1, */
/*       event_ptr->data.note_on.note, */
/*       event_ptr->data.note_on.velocity); */
    break;
  case MIDI_EVENT_KEYPRESS:
    seq_event_ptr->type = SND_SEQ_EVENT_KEYPRESS;
    snd_seq_ev_set_fixed(seq_event_ptr);
    seq_event_ptr->data.note.channel = event_ptr->data.keypress.channel;
    seq_event_ptr->data.note.note = event_ptr->data.keypress.note;
    seq_event_ptr->data.note.velocity = event_ptr->data.keypress.velocity;
/*     DEBUG_OUT( */
/*       "%8u: Channel %u: Keypress %u:%u on", */
/*       event_ptr->tick, */
/*       event_ptr->data.keypress.channel + 1, */
/*       event_ptr->data.keypress.note, */
/*       event_ptr->data.keypress.velocity); */
    break;
  case MIDI_EVENT_CC:
    seq_event_ptr->type = SND_SEQ_EVENT_CONTROLLER;
    snd_seq_ev_set_fixed(seq_event_ptr);
    seq_event_ptr->data.control.channel = event_ptr->data.cc.channel;
    seq_event_ptr->data.control.param = event_ptr->data.cc.controller;
    seq_event_ptr->data.control.value = event_ptr->data.cc.value;
/*     DEBUG_OUT( */
/*       "%8u: Channel %u: Controller %u -> %u", */
/*       event_ptr->tick, */
/*       event_ptr->data.cc.channel + 1, */
/*       event_ptr->data.cc.controller, */
/*       event_ptr->data.cc.value); */
    break;
  case MIDI_EVENT_PITCH:
    seq_event_ptr->type = SND_SEQ_EVENT_PITCHBEND;
    snd_seq_ev_set_fixed(seq_event_ptr);
    seq_event_ptr->data.control.channel = event_ptr->data.pitch.channel;
    seq_event_ptr->data.control.value = (int)(event_ptr->data.pitch.value) - 0x2000;
/*     DEBUG_OUT( */
/*       "%8u: Channel %u: Pitch -> %u", */
/*       event_ptr->tick, */
/*       event_ptr->data.pitch.channel + 1, */
/*       event_ptr->data.pitch.value); */
    break;
  case MIDI_EVENT_PGMCHANGE:
    seq_event_ptr->type = SND_SEQ_EVENT_PGMCHANGE;
    snd_seq_ev_set_fixed(seq_event_ptr);
    seq_event_ptr->data.control.channel = event_ptr->data.pgmchange.channel;
    seq_event_ptr->data.control.value = event_ptr->data.pgmchange.program;
/*     DEBUG_OUT( */
/*       "%8u: Channel %u: Program change -> %u", */
/*       event_ptr->tick, */
/*       event_ptr->data.pgmchange.channel + 1, */
/*       event_ptr->data.pgmchange.program); */
    break;
  case MIDI_EVENT_CHANPRESS:
    seq_event_ptr->type = SND_SEQ_EVENT_CHANPRESS;
    snd_seq_ev_set_fixed(seq_event_ptr);
    seq_event_ptr->data.control.channel = event_ptr->data.chanpress.channel;
    seq_event_ptr->data.control.value = event_ptr->data.chanpress.value;
/*     DEBUG_OUT( */
/*       "%8u: Channel %u: Channel press -> %u", */
/*       event_ptr->tick, */
/*       event_ptr->data.chanpress.channel + 1, */
/*       event_ptr->data.chanpress.value); */
    break;
  case MIDI_EVENT_SYSEX:
    seq_event_ptr->type = SND_SEQ_EVENT_SYSEX;
    snd_seq_ev_set_variable(seq_event_ptr, event_ptr->data.sysex.size, event_ptr->data.sysex.data);
/*     DEBUG_OUT("%8u: SYSEX", event_ptr->tick); */
    break;
  case MIDI_EVENT_TEMPO:
    seq_event_ptr->type = SND_SEQ_EVENT_TEMPO;
    snd_seq_ev_set_fixed(seq_event_ptr);
    seq_event_ptr->dest.client = SND_SEQ_CLIENT_SYSTEM;
    seq_event_ptr->dest.port = SND_SEQ_PORT_SYSTEM_TIMER;
    seq_event_ptr->data.queue.queue = player_ptr->queue;
    seq_event_ptr->data.queue.param.value = event_ptr->data.tempo.value;
/*     DEBUG_OUT( */
/*       "%8u: Tempo change -> %u", */
/*       event_ptr->tick, */
/*       event_ptr->data.tempo.value); */
    break;
  default:
    ERROR_OUT("Invalid event type %d!", event_ptr->type);
    return -1;
  }

  return 0;
}

int
player_alsa_midi_queue_midi(
  struct player_alsa_midi * player_ptr,
  bool prepare)
{
  snd_seq_event_t ev;
  int ret, fillret;
  unsigned int count;

  DEBUG_OUT("player_alsa_midi_queue_midi");

  if (prepare)
  {
    DEBUG_OUT("snd_seq_start_queue");
    ret = snd_seq_start_queue(player_ptr->seq, player_ptr->queue, NULL);
    if (ret < 0)
    {
      ERROR_OUT("Cannot start queue - %s", snd_strerror(ret));
      return -1;
    }

    /* The queue won't be started until the START_QUEUE event is
     * actually drained to the kernel, which is exactly what we want. */
  }

  /* common settings for all our events */
  snd_seq_ev_clear(&ev);
  ev.queue = player_ptr->queue;
  ev.source.port = 0;
  ev.flags = SND_SEQ_TIME_STAMP_TICK;
  ev.time.tick = 0;             /* in case of no events, for end of song event */

  count = 0;

loop:
  fillret = player_alsa_midi_fill_event(player_ptr, &ev);
  if (fillret < 0)
  {
    return -1;
  }

/*   DEBUG_OUT( */
/*     "tick %5d, type %3u, flags %02X, tag %02X, queue %02X", */
/*     (int)ev.time.tick, */
/*     (unsigned int)ev.type, */
/*     (unsigned int)ev.flags, */
/*     (unsigned int)ev.tag, */
/*     (unsigned int)ev.queue); */

  /* this blocks when the output pool has been filled */
  if (prepare)
  {
/*     DEBUG_OUT("Calling snd_seq_event_output_buffer"); */
    ret = snd_seq_event_output_buffer(player_ptr->seq, &ev);
    if (ret == -EAGAIN)
    {
      DEBUG_OUT("Filled %u events, would block", count);
      return 2;                 /* will block */
    }
  }
  else
  {
/*     DEBUG_OUT("Calling snd_seq_event_output"); */
    ret = snd_seq_event_output(player_ptr->seq, &ev);
  }

  pthread_mutex_lock(&player_ptr->feed_mutex);
  if (player_ptr->feed_thread_state == FEED_THREAD_STATE_STOP_REQUESTED)
  {
    pthread_mutex_unlock(&player_ptr->feed_mutex);
    return 2;
  }
  pthread_mutex_unlock(&player_ptr->feed_mutex);

  if (ret < 0)
  {
    ERROR_OUT("Cannot output event - %s", snd_strerror(ret));
    return -1;
  }

  count++;
/*   DEBUG_OUT("%d bytes", ret); */

  if (fillret == 1)
  {
    DEBUG_OUT("Filled %u events, end of data", count);
    return 1;                   /* end of data */
  }

  goto loop;
}

#define player_ptr ((struct player_alsa_midi *)context)

void * player_alsa_midi_feed(void * context)
{
  int ret;

  DEBUG_OUT("player_alsa_midi_feed thread started");

  {
    struct sched_param param;
    int policy;

    pthread_getschedparam(pthread_self(), &policy, &param);
    param.sched_priority = player_ptr->rt_prio_alsa_midi_feed;
    policy = SCHED_FIFO;
    pthread_setschedparam(pthread_self(), policy, &param);
  }

  /* mark that we are ready */
  pthread_mutex_lock(&player_ptr->feed_mutex);

  player_ptr->feed_thread_state = FEED_THREAD_STATE_ARMED;

  ret = pthread_cond_signal(&player_ptr->feed_cond);
  if (ret != 0)
  {
    ERROR_OUT("Failed to signal feed cond (%d)", ret);
    pthread_mutex_unlock(&player_ptr->feed_mutex);
    goto exit;
  }

  pthread_mutex_unlock(&player_ptr->feed_mutex);

  /* wait for play command */
  pthread_mutex_lock(&player_ptr->feed_mutex);

  while (player_ptr->feed_thread_state == FEED_THREAD_STATE_ARMED)
  {
    pthread_cond_wait(&player_ptr->feed_cond, &player_ptr->feed_mutex);
  }

  if (player_ptr->feed_thread_state != FEED_THREAD_STATE_PLAY)
  {
    goto exit_locked;
  }

  pthread_mutex_unlock(&player_ptr->feed_mutex);

  /* make sure that the sequencer sees all our events */
  DEBUG_OUT("INITIAL snd_seq_drain_output ...");
  ret = snd_seq_drain_output(player_ptr->seq);
  DEBUG_OUT("INITIAL snd_seq_drain_output DONE");

  pthread_mutex_lock(&player_ptr->feed_mutex);
  if (player_ptr->feed_thread_state == FEED_THREAD_STATE_STOP_REQUESTED)
  {
    goto exit_locked;
  }
  pthread_mutex_unlock(&player_ptr->feed_mutex);

  if (ret < 0)
  {
    ERROR_OUT("Cannot drain initial output - %s", snd_strerror(ret));
    goto exit;
  }

  if (!player_ptr->eod_source)
  {
    ret = player_alsa_midi_queue_midi(player_ptr, false);
    if (ret < 0)
    {
      DEBUG_OUT("player_alsa_midi_queue_midi() failed.");
      goto exit;
    }

    pthread_mutex_lock(&player_ptr->feed_mutex);
    if (player_ptr->feed_thread_state == FEED_THREAD_STATE_STOP_REQUESTED)
    {
      goto exit_locked;
    }
    pthread_mutex_unlock(&player_ptr->feed_mutex);

    /* make sure that the sequencer sees all our events */
    DEBUG_OUT("snd_seq_drain_output");
    ret = snd_seq_drain_output(player_ptr->seq);

    pthread_mutex_lock(&player_ptr->feed_mutex);
    if (player_ptr->feed_thread_state == FEED_THREAD_STATE_STOP_REQUESTED)
    {
      goto exit_locked;
    }
    pthread_mutex_unlock(&player_ptr->feed_mutex);

    if (ret < 0)
    {
      ERROR_OUT("Cannot drain output - %s", snd_strerror(ret));
      goto exit;
    }
  }

  /*
   * There are three possibilities how to wait until all events have
   * been played:
   * 1) send an event back to us (like pmidi does), and wait for it;
   * 2) wait for the EVENT_STOP notification for our queue which is sent
   *    by the system timer port (this would require a subscription);
   * 3) wait until the output pool is empty.
   * The last is the simplest.
   */

  DEBUG_OUT("snd_seq_sync_output_queue");
  ret = snd_seq_sync_output_queue(player_ptr->seq);
  if (ret < 0)
  {
    ERROR_OUT("Cannot sync output - %s", snd_strerror(ret));
    goto exit;
  }

  player_ptr->eod(player_ptr->eod_context);

exit:
  pthread_mutex_lock(&player_ptr->feed_mutex);

exit_locked:
  player_ptr->feed_thread_state = FEED_THREAD_STATE_STOPPED;

  ret = pthread_cond_signal(&player_ptr->feed_cond);
  if (ret != 0)
  {
    ERROR_OUT("Failed to signal feed cond (%d)", ret);
  }

  pthread_mutex_unlock(&player_ptr->feed_mutex);

  DEBUG_OUT("player_alsa_midi_feed thread stopped");
  return NULL;
}

#undef player_ptr

#define player_ptr ((struct player_alsa_midi *)virtual_ptr)

void
player_alsa_midi_destroy(struct player * virtual_ptr)
{
  int ret;

  DEBUG_OUT("player_alsa_midi_destroy");

  if (player_ptr->seq != NULL)  /* we allocate these resource during start_prepare */
  {
    pthread_mutex_lock(&player_ptr->feed_mutex);
    if (player_ptr->feed_thread_state != FEED_THREAD_STATE_STOPPED)
    {
      DEBUG_OUT("Requesting midi feed thread stop");
      player_ptr->feed_thread_state = FEED_THREAD_STATE_STOP_REQUESTED;

      ret = pthread_cond_signal(&player_ptr->feed_cond);
      if (ret != 0)
      {
        ERROR_OUT("Failed to signal feed cond (%d)", ret);
        pthread_mutex_unlock(&player_ptr->feed_mutex);
        goto destroy_cond;
      }
    }
    pthread_mutex_unlock(&player_ptr->feed_mutex);

    snd_seq_drop_output(player_ptr->seq);
    snd_seq_drain_output(player_ptr->seq);

    DEBUG_OUT("joining midi feed thread...");
    pthread_join(player_ptr->feed_thread, NULL);
    DEBUG_OUT("joined midi feed thread.");

  destroy_cond:
    ret = pthread_cond_destroy(&player_ptr->feed_cond);
    if (ret != 0)
    {
      ERROR_OUT("Failed to destroy feed cond (%d)", ret);
    }

    ret = pthread_mutex_destroy(&player_ptr->feed_mutex);
    if (ret != 0)
    {
      ERROR_OUT("Failed to destroy feed mutex (%d)", ret);
    }

    snd_seq_queue_tempo_free(player_ptr->queue_tempo_ptr);

    DEBUG_OUT("Calling snd_seq_free_queue");
    snd_seq_free_queue(player_ptr->seq, player_ptr->queue);

    DEBUG_OUT("Calling snd_seq_delete_port");
    snd_seq_delete_port(player_ptr->seq, player_ptr->our_port);

    DEBUG_OUT("Calling snd_seq_close");
    snd_seq_close(player_ptr->seq);
  }

  free(player_ptr);
}

int player_alsa_midi_start_prepare(struct player * virtual_ptr)
{
  int ret, ret1;
  snd_seq_port_info_t * pinfo;
  struct midi_event * tempo_event_ptr;
  struct midi_event * ppqn_event_ptr;
  snd_seq_queue_timer_t * timer_info_ptr;

  DEBUG_OUT("player_alsa_midi_start_prepare");

  /* open sequencer */
  DEBUG_OUT("snd_seq_open");
  ret = snd_seq_open(&player_ptr->seq, "default", SND_SEQ_OPEN_OUTPUT, 0);
  if (ret < 0)
  {
    ERROR_OUT("Cannot open sequencer - %s", snd_strerror(ret));
    ret = -1;
    goto exit;
  }

  player_alsa_midi_print_destination_info(player_ptr);
  NOTICE_OUT("MIDI: Using priority %i for feed thread", player_ptr->rt_prio_alsa_midi_feed);

  /* set our name (otherwise it's "Client-xxx") */
  DEBUG_OUT("snd_seq_set_client_name");
  ret = snd_seq_set_client_name(player_ptr->seq, "monster");
  if (ret < 0)
  {
    ERROR_OUT("Cannot set client name - %s", snd_strerror(ret));
    ret = -1;
    goto exit_close_sequencer;
  }

  snd_seq_port_info_alloca(&pinfo);

  /* the first created port is 0 anyway, but let's make sure ... */
  snd_seq_port_info_set_port(pinfo, 0);
  snd_seq_port_info_set_port_specified(pinfo, 1);

  snd_seq_port_info_set_name(pinfo, "monster");

  snd_seq_port_info_set_capability(pinfo, 0); /* sic */
  snd_seq_port_info_set_type(pinfo,
                             SND_SEQ_PORT_TYPE_MIDI_GENERIC |
                             SND_SEQ_PORT_TYPE_APPLICATION);

  DEBUG_OUT("snd_seq_create_port");
  ret = snd_seq_create_port(player_ptr->seq, pinfo);
  if (ret < 0)
  {
    ERROR_OUT("Cannot create port - %s", snd_strerror(ret));
    ret = -1;
    goto exit_close_sequencer;
  }

  player_ptr->our_port = snd_seq_port_info_get_port(pinfo);

  DEBUG_OUT("snd_seq_alloc_named_queue");
  player_ptr->queue = snd_seq_alloc_named_queue(player_ptr->seq, "monster");
  if (player_ptr->queue < 0)
  {
    ERROR_OUT("Cannot create queue - %s", snd_strerror(player_ptr->queue));
    ret = -1;
    goto exit_close_sequencer;
  }

  DEBUG_OUT("queue %u created", (unsigned int)player_ptr->queue);

  /* the queue is now locked, which is just fine */

  snd_seq_queue_timer_alloca(&timer_info_ptr);
  ret = snd_seq_get_queue_timer(player_ptr->seq, player_ptr->queue, timer_info_ptr);
  if (ret != 0)
  {
    ERROR_OUT("snd_seq_get_queue_timer() failed - %s", snd_strerror(ret));
    ret = -1;
    goto exit_free_queue;
  }

  NOTICE_OUT("Using timer %d", snd_seq_queue_timer_get_type(timer_info_ptr));

  /*
   * We send MIDI events with explicit destination addresses, so we don't
   * need any connections to the playback ports.  But we connect to those
   * anyway to force any underlying RawMIDI ports to remain open while
   * we're playing - otherwise, ALSA would reset the port after every
   * event.
   */
  DEBUG_OUT("Connecting to ALSA MIDI port %d:%d", player_ptr->port.client, player_ptr->port.port);
  ret = snd_seq_connect_to(player_ptr->seq, 0, player_ptr->port.client, player_ptr->port.port);
  if (ret < 0)
  {
    ERROR_OUT(
      "Cannot connect to port %d:%d - %s",
      player_ptr->port.client,
      player_ptr->port.port,
      snd_strerror(ret));
    ret = -1;
    goto exit_free_queue;
  }

  ret = media_source_get_frame_data(player_ptr->media_source_ptr, (void **)&ppqn_event_ptr);
  if (ret != 0)
  {
    ERROR_OUT("Cannot read PPQN event");
    ret = -1;
    goto exit_free_queue;
  }

  if (ppqn_event_ptr->type != MIDI_EVENT_PPQN)
  {
    ERROR_OUT("First event is not PPQN");
    ret = -1;
    goto exit_free_queue;
  }

  media_source_next_frame(player_ptr->media_source_ptr);

  ret = media_source_get_frame_data(player_ptr->media_source_ptr, (void **)&tempo_event_ptr);
  if (ret != 0)
  {
    ERROR_OUT("Cannot read tempo event");
    ret = -1;
    goto exit_free_queue;
  }

  if (tempo_event_ptr->type != MIDI_EVENT_TEMPO)
  {
    ERROR_OUT("Second event is not tempo change");
    ret = -1;
    goto exit_free_queue;
  }

  media_source_next_frame(player_ptr->media_source_ptr);

  snd_seq_queue_tempo_set_tempo(player_ptr->queue_tempo_ptr, tempo_event_ptr->data.tempo.value);
  snd_seq_queue_tempo_set_ppq(player_ptr->queue_tempo_ptr, ppqn_event_ptr->data.ppqn.value);

  DEBUG_OUT(
    "Setting queue tempo to %u:%i",
    snd_seq_queue_tempo_get_tempo(player_ptr->queue_tempo_ptr),
    snd_seq_queue_tempo_get_ppq(player_ptr->queue_tempo_ptr));

  ret = snd_seq_set_queue_tempo(player_ptr->seq, player_ptr->queue, player_ptr->queue_tempo_ptr);
  if (ret < 0)
  {
    ERROR_OUT(
      "Cannot set queue tempo (%u/%i)",
      snd_seq_queue_tempo_get_tempo(player_ptr->queue_tempo_ptr),
      snd_seq_queue_tempo_get_ppq(player_ptr->queue_tempo_ptr));
    ret = -1;
    goto exit_free_queue;
  }

  ret = pthread_mutex_init(&player_ptr->feed_mutex, NULL);
  if (ret != 0)
  {
    ERROR_OUT("Failed to init feed mutex (%d)", ret);
    ret = -1;
    goto exit_free_queue;
  }

  ret = pthread_cond_init(&player_ptr->feed_cond, NULL);
  if (ret != 0)
  {
    ERROR_OUT("Failed to init feed cond (%d)", ret);
    ret = -1;
    goto exit_destroy_mutex;
  }

  ret = player_alsa_midi_queue_midi(player_ptr, true);
  if (ret < 0)
  {
    goto exit_free_queue;
  }

  player_ptr->feed_thread_state = FEED_THREAD_STATE_INITIAL;

  ret = pthread_create(&player_ptr->feed_thread, NULL, player_alsa_midi_feed, player_ptr);
  if (ret != 0)
  {
    ERROR_OUT("Failed to start feed thread (%d)", ret);
    ret = -1;
    goto exit_destroy_cond;
  }

  DEBUG_OUT("Waiting MIDI feed thread to arm...");
  pthread_mutex_lock(&player_ptr->feed_mutex);
  while (player_ptr->feed_thread_state == FEED_THREAD_STATE_INITIAL)
  {
    pthread_cond_wait(&player_ptr->feed_cond, &player_ptr->feed_mutex);
  }

  if (player_ptr->feed_thread_state != FEED_THREAD_STATE_ARMED)
  {
    ERROR_OUT("Feed thread failed to prefill kernel buffers");
    pthread_mutex_unlock(&player_ptr->feed_mutex);
    goto exit_join;
  }

  pthread_mutex_unlock(&player_ptr->feed_mutex);

  ret = 0;
  goto exit;

exit_join:
  /* signal thread that it must stop */
  pthread_mutex_lock(&player_ptr->feed_mutex);
  if (player_ptr->feed_thread_state != FEED_THREAD_STATE_STOPPED)
  {
    player_ptr->feed_thread_state = FEED_THREAD_STATE_STOP_REQUESTED;

    ret = pthread_cond_signal(&player_ptr->feed_cond);
    if (ret != 0)
    {
      ERROR_OUT("Failed to signal feed cond (%d)", ret);
      pthread_mutex_unlock(&player_ptr->feed_mutex);
      goto exit_destroy_cond;
    }
  }
  pthread_mutex_unlock(&player_ptr->feed_mutex);

  ret = pthread_join(player_ptr->feed_thread, NULL);
  if (ret != 0)
  {
    ERROR_OUT("Failed to join feed thread (%d)", ret);
  }

exit_destroy_cond:
  ret1 = pthread_cond_destroy(&player_ptr->feed_cond);
  if (ret1 != 0)
  {
    ERROR_OUT("Failed to destroy feed cond (%d)", ret1);
  }

exit_destroy_mutex:
  ret1 = pthread_mutex_destroy(&player_ptr->feed_mutex);
  if (ret1 != 0)
  {
    ERROR_OUT("Failed to destroy feed mutex (%d)", ret1);
  }

exit_free_queue:
  snd_seq_free_queue(player_ptr->seq, player_ptr->queue);

exit_close_sequencer:
  DEBUG_OUT("snd_seq_close");
  snd_seq_close(player_ptr->seq);
  player_ptr->seq = NULL;

exit:
  return ret;
}

int player_alsa_midi_start(struct player * virtual_ptr)
{
  int ret;

  DEBUG_OUT("player_alsa_midi_start");

  /* signal thread that it must start play */

  pthread_mutex_lock(&player_ptr->feed_mutex);

  if (player_ptr->feed_thread_state != FEED_THREAD_STATE_ARMED)
  {
    ERROR_OUT("Feed thread is not armed");
    ret = -1;
    goto unlock;
  }

  player_ptr->feed_thread_state = FEED_THREAD_STATE_PLAY;

  ret = pthread_cond_signal(&player_ptr->feed_cond);
  if (ret != 0)
  {
    ERROR_OUT("Failed to signal feed cond (%d)", ret);
    ret = -1;
    goto unlock;
  }

  ret = 0;

unlock:
  pthread_mutex_unlock(&player_ptr->feed_mutex);

  return ret;
}

#undef player_ptr

int
player_alsa_midi(
  struct media_source * media_source_ptr,
  eod_t eod,
  unsigned int eod_context,
  struct player ** player_ptr_ptr)
{
  int ret;
  struct player_alsa_midi * player_ptr;
  int client;
  int port;
  unsigned int rt_prio_alsa_midi_feed;

  DEBUG_OUT("player_alsa_midi");

  ret = conf_file_get_int("/conf/midi/client", &client);
  if (ret != 0)
  {
    ERROR_OUT("failed to read from configuration which ALSA midi client to send data to");
    ret = -1;
    goto exit;
  }

  ret = conf_file_get_int("/conf/midi/port", &port);
  if (ret != 0)
  {
    ERROR_OUT("failed to read from configuration which ALSA midi client port to send data to");
    ret = -1;
    goto exit;
  }

  ret = conf_file_get_uint("/conf/realtime_priorities/alsa_midi_feed", &rt_prio_alsa_midi_feed);
  if (ret != 0)
  {
    ERROR_OUT("failed to read from configuration what realtime priority to use for ALSA MIDI feed thread");
    ret = -1;
    goto exit;
  }

  player_ptr = malloc(sizeof(struct player_alsa_midi));
  if (player_ptr == NULL)
  {
    ERROR_OUT("malloc() failed.");
    ret = -1;
    goto exit;
  }

  player_ptr->virtual.destroy = player_alsa_midi_destroy;
  player_ptr->virtual.start_prepare = player_alsa_midi_start_prepare;
  player_ptr->virtual.start = player_alsa_midi_start;

  player_ptr->media_source_ptr = media_source_ptr;

  player_ptr->eod = eod;
  player_ptr->eod_context = eod_context;

  player_ptr->port.client = client;
  player_ptr->port.port = port;
  player_ptr->rt_prio_alsa_midi_feed = rt_prio_alsa_midi_feed;

  player_ptr->eod_source = false;

  ret = snd_seq_queue_tempo_malloc(&player_ptr->queue_tempo_ptr);
  if (ret < 0)
  {
    ERROR_OUT("snd_seq_queue_tempo_malloc() failed.");
    goto free_player;
  }

  player_ptr->seq = NULL;

  strcpy(player_ptr->virtual.name, "ALSA MIDI");

  *player_ptr_ptr = &player_ptr->virtual;

  ret = 0;
  goto exit;

free_player:
  free(player_ptr);

exit:
  return ret;
}
