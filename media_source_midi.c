/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 *   Media source implementation that reads MIDI files.
 *   This file is part of monster
 *
 *   Copyright (C) 2006,2007 Nedko Arnaudov <nedko@arnaudov.name>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; version 2 of the License
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *****************************************************************************/

#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>

#define DISABLE_DEBUG_OUTPUT

#include "media_source.h"
#include "media_source_midi.h"
#include "log.h"
#include "midi_event.h"

/*
 * A MIDI event after being parsed/loaded from the file.
 * There could be made a case for using snd_seq_event_t instead.
 */
struct event
{
  struct event * next_ptr;   /* linked list */

  struct midi_event event;
  unsigned char sysex[0];
};

struct track
{
  struct event * first_event_ptr; /* list of all events in this track */
  int end_tick;                 /* length of this track */

  struct event * current_event_ptr;
};

struct media_source_midi
{
  struct media_source virtual;

  FILE * file_handle;
  int file_offset;   /* current offset in input file */
  int num_tracks;
  struct track * tracks;
  int smpte_timing;
  signed int max_tick;
  struct event * ppqn_event_ptr;
  struct event * tempo_event_ptr;
  struct event * event_ptr;
  unsigned int counter;
};

static void cleanup_file_data(struct media_source_midi * media_source_ptr)
{
  int i;
  struct event * event_ptr;

  if (media_source_ptr->tracks != NULL)
  {
    for (i = 0; i < media_source_ptr->num_tracks; ++i)
    {
      event_ptr = media_source_ptr->tracks[i].first_event_ptr;
      while (event_ptr != NULL)
      {
        struct event * next_ptr = event_ptr->next_ptr;
        free(event_ptr);
        event_ptr = next_ptr;
      }
    }

    media_source_ptr->num_tracks = 0;

    free(media_source_ptr->tracks);
  }

  if (media_source_ptr->ppqn_event_ptr != NULL)
  {
    free(media_source_ptr->ppqn_event_ptr);
  }

  if (media_source_ptr->tempo_event_ptr != NULL)
  {
    free(media_source_ptr->tempo_event_ptr);
  }

  media_source_ptr->tracks = NULL;
}

#define media_source_ptr ((struct media_source_midi *)virtual_ptr)

void media_source_midi_destroy(struct media_source * virtual_ptr)
{
  DEBUG_OUT("media_source_midi_destroy");
  cleanup_file_data(media_source_ptr);
  fclose(media_source_ptr->file_handle);
  free(media_source_ptr);
}

int media_source_midi_format_query(struct media_source * virtual_ptr, unsigned int * format_ptr)
{
  DEBUG_OUT("media_source_midi_format_query");
  *format_ptr = MEDIA_FORMAT_MIDI_EVENT;
  return 0;
}

int media_source_midi_get_frame_data(struct media_source * virtual_ptr, void ** buffer_ptr_ptr)
{
  DEBUG_OUT("media_source_midi_get_frame_data");

  if (media_source_ptr->counter == 0)
  {
    if (media_source_ptr->ppqn_event_ptr == NULL)
    {
      ERROR_OUT("No PPQN event!");
      return -1;
    }

    *buffer_ptr_ptr = &media_source_ptr->ppqn_event_ptr->event;
    DEBUG_OUT("Returning the PPQN event");
    return 0;
  }

  if (media_source_ptr->counter == 1)
  {
    if (media_source_ptr->tempo_event_ptr == NULL)
    {
      ERROR_OUT("No tempo event!");
      return -1;
    }

    *buffer_ptr_ptr = &media_source_ptr->tempo_event_ptr->event;
    DEBUG_OUT("Returning the tempo event");
    return 0;
  }

  if (media_source_ptr->event_ptr == NULL)
  {
    *buffer_ptr_ptr = NULL;
    return 1;
  }

  *buffer_ptr_ptr = &media_source_ptr->event_ptr->event;

  return 0;
}

void
media_source_midi_next_frame(struct media_source * virtual_ptr)
{
  struct event * event_ptr;
  struct track * event_track_ptr;
  int i;
  signed int min_tick;

  DEBUG_OUT("media_source_midi_next_frame");

  media_source_ptr->counter++;

  if (media_source_ptr->counter < 2)
  {
    return;
  }

  event_ptr = NULL;
  event_track_ptr = NULL;
  min_tick = media_source_ptr->max_tick + 1;

  //DEBUG_OUT("Search next event, %d tracks, max_tick = %d, min_tick = %d", media_source_ptr->num_tracks, media_source_ptr->max_tick, min_tick);

  /* search next event */
  for (i = 0; i < media_source_ptr->num_tracks; i++)
  {
    struct track * track = media_source_ptr->tracks + i;
    struct event * e2 = track->current_event_ptr;

    //DEBUG_OUT("Event %08X, tick = %u", (unsigned int)e2, e2 ? e2->event.tick : 0);
    if (e2 && e2->event.tick < min_tick)
    {
      min_tick = e2->event.tick;
      event_ptr = e2;
      event_track_ptr = track;
    }
  }

  if (event_ptr != NULL)        /* if end of song NOT reached */
  {
    /* advance pointer to next event */
    event_track_ptr->current_event_ptr = event_ptr->next_ptr;
  }
  else
  {
    DEBUG_OUT("End of song");
  }

  media_source_ptr->event_ptr = event_ptr;
}

#undef media_source_ptr

static int read_byte(struct media_source_midi * media_source_ptr)
{
  media_source_ptr->file_offset++;
  return getc(media_source_ptr->file_handle);
}

/* reads a little-endian 32-bit integer */
static int read_32_le(struct media_source_midi * media_source_ptr)
{
  int value;
  value = read_byte(media_source_ptr);
  value |= read_byte(media_source_ptr) << 8;
  value |= read_byte(media_source_ptr) << 16;
  value |= read_byte(media_source_ptr) << 24;
  return !feof(media_source_ptr->file_handle) ? value : -1;
}

/* reads a 4-character identifier */
static int read_id(struct media_source_midi * media_source_ptr)
{
  int id;

  id = read_32_le(media_source_ptr);

  DEBUG_OUT(
    "Chunk \"%c%c%c%c\"",
    (char)(id & 0xFF),
    (char)((id >> 8) & 0xFF),
    (char)((id >> 16) & 0xFF),
    (char)((id >> 24) & 0xFF));

  return id;
}

#define MAKE_ID(c1, c2, c3, c4) ((c1) | ((c2) << 8) | ((c3) << 16) | ((c4) << 24))

/* reads a fixed-size big-endian number */
static int read_int(struct media_source_midi * media_source_ptr, int bytes)
{
  int c, value = 0;

  do
  {
    c = read_byte(media_source_ptr);
    if (c == EOF)
      return -1;
    value = (value << 8) | c;
  }
  while (--bytes);

  return value;
}

/* reads a variable-length number */
static int read_var(struct media_source_midi * media_source_ptr)
{
  int value, c;

  c = read_byte(media_source_ptr);
  value = c & 0x7f;
  if (c & 0x80)
  {
    c = read_byte(media_source_ptr);
    value = (value << 7) | (c & 0x7f);
    if (c & 0x80)
    {
      c = read_byte(media_source_ptr);
      value = (value << 7) | (c & 0x7f);
      if (c & 0x80)
      {
        c = read_byte(media_source_ptr);
        value = (value << 7) | c;
        if (c & 0x80)
          return -1;
      }
    }
  }
  return !feof(media_source_ptr->file_handle) ? value : -1;
}

/* allocates a new event */
static
int
new_event(struct track * track_ptr, int sysex_length, struct event ** event_ptr_ptr)
{
  struct event * event_ptr;

  event_ptr = malloc(sizeof(struct event) + sysex_length);
  if (event_ptr == NULL)
  {
    ERROR_OUT("malloc failed.");
    return -1;
  }

  event_ptr->next_ptr = NULL;

  /* append at the end of the track's linked list */
  if (track_ptr->current_event_ptr != NULL)
  {
    track_ptr->current_event_ptr->next_ptr = event_ptr;
  }
  else
  {
    track_ptr->first_event_ptr = event_ptr;
  }

  track_ptr->current_event_ptr = event_ptr;

  *event_ptr_ptr = event_ptr;

  return 0;
}

static void skip(struct media_source_midi * media_source_ptr, int bytes)
{
  while (bytes > 0)
    read_byte(media_source_ptr), --bytes;
}

/* reads one complete track from the file */
static int read_track(struct media_source_midi * media_source_ptr, struct track *track, int track_end)
{
  int ret;
  int tick = 0;
  unsigned char last_cmd = 0;

  /* the current file position is after the track ID and length */
  while (media_source_ptr->file_offset < track_end)
  {
    unsigned char cmd;
    struct event * event_ptr;
    int delta_ticks, len, c;

    delta_ticks = read_var(media_source_ptr);
    if (delta_ticks < 0)
      break;
    tick += delta_ticks;

    c = read_byte(media_source_ptr);
    if (c < 0)
      break;

    if (c & 0x80)
    {
      /* have command */
      cmd = c;
      if (cmd < 0xf0)
        last_cmd = cmd;
    }
    else
    {
      /* running status */
      ungetc(c, media_source_ptr->file_handle);
      media_source_ptr->file_offset--;
      cmd = last_cmd;
      if (!cmd)
        goto _error;
    }

    switch (cmd >> 4)
    {
      /* channel messages with 2 parameter bytes */
    case 0x8:                   /* Note Off */
      ret = new_event(track, 0, &event_ptr);
      if (ret < 0)
      {
        return -1;
      }

      event_ptr->event.type = MIDI_EVENT_NOTE_OFF;
      event_ptr->event.tick = tick;
      event_ptr->event.data.note_off.channel = cmd & 0x0f;
      event_ptr->event.data.note_off.note = read_byte(media_source_ptr) & 0x7f;
      event_ptr->event.data.note_off.velocity = read_byte(media_source_ptr) & 0x7f;
      break;
    case 0x9:                   /* Note On */
      ret = new_event(track, 0, &event_ptr);
      if (ret < 0)
      {
        return -1;
      }

      event_ptr->event.type = MIDI_EVENT_NOTE_ON;
      event_ptr->event.tick = tick;
      event_ptr->event.data.note_on.channel = cmd & 0x0f;
      event_ptr->event.data.note_on.note = read_byte(media_source_ptr) & 0x7f;
      event_ptr->event.data.note_on.velocity = read_byte(media_source_ptr) & 0x7f;
      break;
    case 0xa:                   /* Polyphonic Key Pressure (Aftertouch) */
      ret = new_event(track, 0, &event_ptr);
      if (ret < 0)
      {
        return -1;
      }

      event_ptr->event.type = MIDI_EVENT_KEYPRESS;
      event_ptr->event.tick = tick;
      event_ptr->event.data.keypress.channel = cmd & 0x0f;
      event_ptr->event.data.keypress.note = read_byte(media_source_ptr) & 0x7f;
      event_ptr->event.data.keypress.velocity = read_byte(media_source_ptr) & 0x7f;
      break;
    case 0xb:                   /* Control Change */
      ret = new_event(track, 0, &event_ptr);
      if (ret < 0)
      {
        return -1;
      }

      event_ptr->event.type = MIDI_EVENT_CC;
      event_ptr->event.tick = tick;
      event_ptr->event.data.cc.channel = cmd & 0x0f;
      event_ptr->event.data.cc.controller = read_byte(media_source_ptr) & 0x7f;
      event_ptr->event.data.cc.value = read_byte(media_source_ptr) & 0x7f;
      break;
    case 0xe:                   /* Pitch Wheel Change */
      ret = new_event(track, 0, &event_ptr);
      if (ret < 0)
      {
        return -1;
      }

      event_ptr->event.type = MIDI_EVENT_CC;
      event_ptr->event.tick = tick;
      event_ptr->event.data.pitch.channel = cmd & 0x0f;
      event_ptr->event.data.pitch.value = (read_byte(media_source_ptr) & 0x7f) | ((read_byte(media_source_ptr) & 0x7f) << 7);
      break;

      /* channel messages with 1 parameter byte */
    case 0xc:                   /* Program Change */
      ret = new_event(track, 0, &event_ptr);
      if (ret < 0)
      {
        return -1;
      }

      event_ptr->event.type = MIDI_EVENT_PGMCHANGE;
      event_ptr->event.tick = tick;
      event_ptr->event.data.pgmchange.channel = cmd & 0x0f;
      event_ptr->event.data.pgmchange.program = read_byte(media_source_ptr) & 0x7f;
      break;
    case 0xd:                   /* Channel Pressure (After-touch) */
      ret = new_event(track, 0, &event_ptr);
      if (ret < 0)
      {
        return -1;
      }

      event_ptr->event.type = MIDI_EVENT_CHANPRESS;
      event_ptr->event.tick = tick;
      event_ptr->event.data.chanpress.channel = cmd & 0x0f;
      event_ptr->event.data.chanpress.value = read_byte(media_source_ptr) & 0x7f;
      break;

    case 0xf:
      switch (cmd)
      {
      case 0xf0: /* sysex */
      case 0xf7: /* continued sysex, or escaped commands */
        len = read_var(media_source_ptr);

        if (len < 0)
          goto _error;

        if (cmd == 0xf0)
          ++len;

        ret = new_event(track, len, &event_ptr);
        if (ret < 0)
        {
          return -1;
        }

        event_ptr->event.type = MIDI_EVENT_SYSEX;
        event_ptr->event.tick = tick;
        event_ptr->event.data.sysex.size = len;
        event_ptr->event.data.sysex.data = event_ptr->sysex;
        if (cmd == 0xf0)
        {
          event_ptr->sysex[0] = 0xf0;
          c = 1;
        }
        else
        {
          c = 0;
        }
        for (; c < len; ++c)
          event_ptr->sysex[c] = read_byte(media_source_ptr);
        break;

      case 0xff: /* meta event */
        c = read_byte(media_source_ptr);
        len = read_var(media_source_ptr);
        if (len < 0)
          goto _error;

        switch (c)
        {
        case 0x21: /* port number */
          if (len < 1)
            goto _error;
          {
            int port;
            port = read_byte(media_source_ptr);
            DEBUG_OUT("port %u", (unsigned int)port);
            skip(media_source_ptr, len - 1);
          }
          break;

        case 0x2f: /* end of track */
          track->end_tick = tick;
          skip(media_source_ptr, track_end - media_source_ptr->file_offset);
          return 0;

        case 0x51: /* tempo */
          if (len < 3)
            goto _error;
          if (media_source_ptr->smpte_timing)
          {
            /* SMPTE timing doesn't change */
            skip(media_source_ptr, len);
          }
          else
          {
            ret = new_event(track, 0, &event_ptr);
            if (ret < 0)
            {
              return -1;
            }

            event_ptr->event.type = MIDI_EVENT_TEMPO;
            event_ptr->event.tick = tick;
            event_ptr->event.data.tempo.value = read_byte(media_source_ptr) << 16;
            event_ptr->event.data.tempo.value |= read_byte(media_source_ptr) << 8;
            event_ptr->event.data.tempo.value |= read_byte(media_source_ptr);
            skip(media_source_ptr, len - 3);
          }
          break;

        default: /* ignore all other meta events */
          skip(media_source_ptr, len);
          break;
        }
        break;

      default: /* invalid Fx command */
        goto _error;
      }
      break;

    default: /* cannot happen */
      goto _error;
    }
  }
_error:
  ERROR_OUT("invalid MIDI data (offset %#x)", media_source_ptr->file_offset);
  return -1;
}

int
set_ppqn_event(
  struct media_source_midi * media_source_ptr,
  unsigned int ppqn)
{
  media_source_ptr->ppqn_event_ptr = malloc(sizeof(struct event));
  if (media_source_ptr->ppqn_event_ptr == NULL)
  {
    ERROR_OUT("malloc failed.");
    return -1;
  }

  media_source_ptr->ppqn_event_ptr->event.type = MIDI_EVENT_PPQN;
  media_source_ptr->ppqn_event_ptr->event.tick = 0;
  media_source_ptr->ppqn_event_ptr->event.data.ppqn.value = ppqn;

  return 0;
}

int
set_tempo_event(
  struct media_source_midi * media_source_ptr,
  unsigned int tempo)
{
  media_source_ptr->tempo_event_ptr = malloc(sizeof(struct event));
  if (media_source_ptr->tempo_event_ptr == NULL)
  {
    ERROR_OUT("malloc failed.");
    return -1;
  }


  media_source_ptr->tempo_event_ptr->event.type = MIDI_EVENT_TEMPO;
  media_source_ptr->tempo_event_ptr->event.tick = 0;
  media_source_ptr->tempo_event_ptr->event.data.tempo.value = tempo;

  return 0;
}

/* reads an entire MIDI file */
static int
read_smf(
  struct media_source_midi * media_source_ptr)
{
  int header_len, type, time_division, i;
  int ret;

  /* the curren position is immediately after the "MThd" id */
  header_len = read_int(media_source_ptr, 4);
  if (header_len != 6)
  {
    ERROR_OUT("MIDI parsing error (MThd check)");
    return -1;
  }

  type = read_int(media_source_ptr, 2);

  DEBUG_OUT("Type %d", type);

  if (type != 0 && type != 1)
  {
    ERROR_OUT("MIDI type %d format is not supported", type);
    return -1;
  }

  media_source_ptr->num_tracks = read_int(media_source_ptr, 2);
  if (media_source_ptr->num_tracks < 1 || media_source_ptr->num_tracks > 1000)
  {
    ERROR_OUT("invalid number of tracks (%d)", media_source_ptr->num_tracks);
    media_source_ptr->num_tracks = 0;
    return -1;
  }

  DEBUG_OUT("Tracks count: %d", media_source_ptr->num_tracks);

  media_source_ptr->tracks = calloc(media_source_ptr->num_tracks, sizeof(struct track));
  if (!media_source_ptr->tracks)
  {
    ERROR_OUT("out of memory");
    media_source_ptr->num_tracks = 0;
    return -1;
  }

  time_division = read_int(media_source_ptr, 2);
  if (time_division < 0)
  {
    ERROR_OUT("MIDI parsing error (time division is negative)");
    return -1;
  }

  /* interpret and set tempo */
  media_source_ptr->smpte_timing = !!(time_division & 0x8000);
  if (!media_source_ptr->smpte_timing)
  {
    DEBUG_OUT("PPQN: %d", time_division);
    DEBUG_OUT("Settings default tempo, 120 bpm");
    /* time_division is ticks per quarter */

    ret = set_ppqn_event(media_source_ptr, time_division);
    if (ret < 0)
    {
      return -1;
    }

    ret = set_tempo_event(media_source_ptr, 500000); /* default: 120 bpm */
    if (ret < 0)
    {
      return -1;
    }
  }
  else
  {
    /* upper byte is negative frames per second */
    i = 0x80 - ((time_division >> 8) & 0x7f);
    /* lower byte is ticks per frame */
    time_division &= 0xff;
    /* now pretend that we have quarter-note based timing */
    switch (i)
    {
    case 24:
      ret = set_tempo_event(media_source_ptr, 12 * time_division);
      if (ret < 0)
      {
        return -1;
      }

      ret = set_ppqn_event(media_source_ptr, 500000);
      if (ret < 0)
      {
        return -1;
      }
      break;
    case 25:
      ret = set_tempo_event(media_source_ptr, 10 * time_division);
      if (ret < 0)
      {
        return -1;
      }

      ret = set_ppqn_event(media_source_ptr, 400000);
      if (ret < 0)
      {
        return -1;
      }
      break;
    case 29: /* 30 drop-frame */
      ret = set_tempo_event(media_source_ptr, 2997 * time_division);
      if (ret < 0)
      {
        return -1;
      }

      ret = set_ppqn_event(media_source_ptr, 100000000);
      if (ret < 0)
      {
        return -1;
      }
      break;
    case 30:
      ret = set_tempo_event(media_source_ptr, 15 * time_division);
      if (ret < 0)
      {
        return -1;
      }

      ret = set_ppqn_event(media_source_ptr, 500000);
      if (ret < 0)
      {
        return -1;
      }
      break;
    default:
      ERROR_OUT("invalid number of SMPTE frames per second (%d)", i);
      return -1;
    }
  }

  /* read tracks */
  for (i = 0; i < media_source_ptr->num_tracks; ++i)
  {
    int len;

    /* search for MTrk chunk */
    for (;;)
    {
      int id = read_id(media_source_ptr);
      len = read_int(media_source_ptr, 4);
      if (feof(media_source_ptr->file_handle))
      {
        ERROR_OUT("unexpected end of file");
        return -1;
      }
      if (len < 0 || len >= 0x10000000)
      {
        ERROR_OUT("invalid chunk length %d", len);
        return -1;
      }
      if (id == MAKE_ID('M', 'T', 'r', 'k'))
        break;
      skip(media_source_ptr, len);
    }

    ret = read_track(media_source_ptr, &media_source_ptr->tracks[i], media_source_ptr->file_offset + len);
    if (ret < 0)
      return -1;
  }

  return 0;
}

int
media_source_midi_load(struct media_source_midi * media_source_ptr)
{
  int i;
  int ret;

  ret = read_smf(media_source_ptr);
  if (ret < 0)
  {
    return -1;
  }

  /* calculate length of the entire file */
  DEBUG_OUT("Finding max_tick....");
  media_source_ptr->max_tick = -1;
  for (i = 0; i < media_source_ptr->num_tracks; ++i)
  {
    DEBUG_OUT("track i end_tick = %d", media_source_ptr->tracks[i].end_tick);
    if (media_source_ptr->tracks[i].end_tick > media_source_ptr->max_tick)
    {
      media_source_ptr->max_tick = media_source_ptr->tracks[i].end_tick;
    }
  }
  DEBUG_OUT("max_tick = %d", media_source_ptr->max_tick);

  /* initialize current position in each track */
  for (i = 0; i < media_source_ptr->num_tracks; ++i)
  {
    media_source_ptr->tracks[i].current_event_ptr = media_source_ptr->tracks[i].first_event_ptr;
  }

  return 0;
}

int
media_source_midi(
  const char * filename,
  struct media_source ** media_source_ptr_ptr)
{
  struct media_source_midi * media_source_ptr;
  int ret;

  DEBUG_OUT("media_source_midi");

  media_source_ptr = (struct media_source_midi *)malloc(sizeof(struct media_source_midi));
  if (media_source_ptr == NULL)
  {
    ERROR_OUT("malloc() failed.");
    ret = -1;
    goto exit;
  }

  media_source_ptr->ppqn_event_ptr = NULL;
  media_source_ptr->tempo_event_ptr = NULL;
  media_source_ptr->tracks = NULL;

  media_source_ptr->file_handle = fopen(filename, "rb");
  if (!media_source_ptr->file_handle)
  {
    ERROR_OUT("Cannot open %s - %s", filename, strerror(errno));
    ret = -1;
    goto cleanup;
  }

  media_source_ptr->file_offset = 0;

  if (read_id(media_source_ptr) != MAKE_ID('M', 'T', 'h', 'd'))
  {
    ERROR_OUT("%s is not a Standard MIDI File", filename);
    ret = -1;
    goto cleanup;
  }

  media_source_ptr->virtual.destroy = media_source_midi_destroy;
  media_source_ptr->virtual.format_query = media_source_midi_format_query;
  media_source_ptr->virtual.get_frame_data = media_source_midi_get_frame_data;
  media_source_ptr->virtual.next_frame = media_source_midi_next_frame;

  ret = media_source_midi_load(media_source_ptr);
  if (ret < 0)
  {
    ERROR_OUT("media_source_midi_load() failed.");
    goto cleanup;
  }

  media_source_ptr->counter = 0;

  *media_source_ptr_ptr = &media_source_ptr->virtual;

  ret = 0;
  goto exit;

cleanup:
  cleanup_file_data(media_source_ptr);
  goto exit;

exit:
  return ret;
}
