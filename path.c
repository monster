/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 *   Filesystem path helpers
 *   This file is part of monster
 *
 *   Copyright (C) 2006,2007 Nedko Arnaudov <nedko@arnaudov.name>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; version 2 of the License
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *****************************************************************************/

#include <string.h>
#include <assert.h>

#define DISABLE_DEBUG_OUTPUT

#include "log.h"
#include "path.h"

void
path_normalize(char * path_ptr)
{
  char * temp_ptr;
  unsigned int dirs_available;
  size_t left;
  char * previous_dir_ptr;

  DEBUG_OUT("Normalizing filesystem path \"%s\"", path_ptr);

  temp_ptr = path_ptr;
  dirs_available = 0;
  left = strlen(temp_ptr)+1;

loop:
  //DEBUG_OUT("%u chars left to process", left);

  if (*temp_ptr == 0)
    goto done;

  if ((temp_ptr == path_ptr || temp_ptr[-1] == '/') &&
      temp_ptr[0] == '.' &&
      temp_ptr[1] == '/')
  {
    DEBUG_OUT("");
    DEBUG_OUT("Skipping '.' from      \"%s\"", temp_ptr);
    memmove(temp_ptr, temp_ptr + 2, left);
    DEBUG_OUT("Skipping '.' result is \"%s\"", temp_ptr);
    DEBUG_OUT("Skipping '.' global result is \"%s\"", path_ptr);
    left -= 2;
    goto loop;
  }

  if (temp_ptr[0] == '.' &&
      temp_ptr[1] == '.' &&
      temp_ptr[2] == '/' &&
      dirs_available > 0)
  {
    DEBUG_OUT("");
    DEBUG_OUT("Skipping '..' from      \"%s\"", temp_ptr);

    assert(temp_ptr > path_ptr + 2);

    previous_dir_ptr = temp_ptr - 2;
    while (previous_dir_ptr > path_ptr &&
           *previous_dir_ptr != '/')
    {
      previous_dir_ptr--;
    }

    if (*previous_dir_ptr == '/')
    {
      previous_dir_ptr++;
    }

    DEBUG_OUT("Previous dir \"%s\"", previous_dir_ptr);

    left -= 3;
    memmove(previous_dir_ptr, temp_ptr + 3, left);
    temp_ptr = previous_dir_ptr;

    dirs_available--;

    DEBUG_OUT("Skipping '..' result is \"%s\"", temp_ptr);
    DEBUG_OUT("Skipping '..' global result is \"%s\"", path_ptr);

    goto loop;
  }

  if (temp_ptr > path_ptr &&
      *temp_ptr == '/')
  {
    dirs_available++;
  }

  temp_ptr++;
  left--;
  goto loop;

done:
  DEBUG_OUT("Filesystem path normalized to \"%s\"", path_ptr);
  assert(left == 1);          /* the terminating zero */
}
