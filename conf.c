/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 *   Configuration file access
 *   This file is part of monster
 *
 *   Copyright (C) 2006,2007 Nedko Arnaudov <nedko@arnaudov.name>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; version 2 of the License
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *****************************************************************************/

#include <libxml/parser.h>

#define DISABLE_DEBUG_OUTPUT

#include "conf.h"
#include "log.h"
#include "xml.h"

xmlDocPtr g_conf_ptr;

int
conf_file_parse()
{
  DEBUG_OUT("conf_file_parse() called.");

  g_conf_ptr = xmlParseFile("/etc/monster.xml");
  if (g_conf_ptr == NULL)
  {
    ERROR_OUT("Failed to parse conf file");
    return -1;
  }

  return 0;
}

void
conf_file_cleanup()
{
  DEBUG_OUT("conf_file_cleanup() called.");
  xmlFreeDoc(g_conf_ptr);
}

int
conf_file_get_string(
  const char * xpath,
  char * buffer_ptr,
  size_t buffer_size)
{
  DEBUG_OUT("conf_file_get_string() called.");
  return xml_get_string(g_conf_ptr, xpath, buffer_ptr, buffer_size);
}

int
conf_file_get_int(
  const char * xpath,
  int * value_ptr)
{
  DEBUG_OUT("conf_file_get_int() called.");
  return xml_get_int(g_conf_ptr, xpath, value_ptr);
}

int
conf_file_get_uint(
  const char * xpath,
  unsigned int * value_ptr)
{
  DEBUG_OUT("conf_file_get_uint() called.");
  return xml_get_uint(g_conf_ptr, xpath, value_ptr);
}
