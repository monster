/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 *   Interface to configuration file
 *   This file is part of monster
 *
 *   Copyright (C) 2006,2007 Nedko Arnaudov <nedko@arnaudov.name>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; version 2 of the License
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *****************************************************************************/

#ifndef CONF_H__865B0AC5_8A54_4782_BE49_88CC3DDABD50__INCLUDED
#define CONF_H__865B0AC5_8A54_4782_BE49_88CC3DDABD50__INCLUDED

int
conf_file_parse();

void
conf_file_cleanup();

int
conf_file_get_string(
  const char * xpath,
  char * buffer_ptr,
  size_t buffer_size);

int
conf_file_get_int(
  const char * xpath,
  int * value_ptr);

int
conf_file_get_uint(
  const char * xpath,
  unsigned int * value_ptr);

#endif /* #ifndef CONF_H__865B0AC5_8A54_4782_BE49_88CC3DDABD50__INCLUDED */
