/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 *   Player capable to send DV frame sequence over FireWire
 *   This file is part of monster
 *
 *   Copyright (C) 2006,2007 Nedko Arnaudov <nedko@arnaudov.name>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; version 2 of the License
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *****************************************************************************/

#define _FILE_OFFSET_BITS 64

#include <stdio.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <unistd.h>
#include <fcntl.h>
#include <sched.h>
#include <sys/resource.h>
#include <string.h>
#include <pthread.h>
#include <stdlib.h>
#include <errno.h>

#define DISABLE_DEBUG_OUTPUT

#include "media_source.h"
#include "player.h"
#include "player_video1394.h"
#include "log.h"
#include "video1394.h"
#include "types.h"
#include "dv.h"
#include "conf.h"

/*------------------------------------- */

#define FEED_THREAD_STATE_INITIAL         0
#define FEED_THREAD_STATE_ARMED           1
#define FEED_THREAD_STATE_PLAY            2
#define FEED_THREAD_STATE_STOP_REQUESTED  3
#define FEED_THREAD_STATE_STOPPED         4

struct player_video1394
{
  struct player virtual;

  struct media_source * media_source_ptr;
  eod_t eod;
  unsigned int eod_context;
  bool pal;

  char video1394_device_path[1024];
  unsigned int channel;
  unsigned int kbuffers_count;
  unsigned int syt_offset;

  int video1394_handle;
  unsigned char * kbuffers_ptr;
  unsigned int packets_count;
  unsigned int * packet_sizes;  /* kbuffers_count * (TARGETBUFSIZE + 1) * sizeof(unsigned int) */
  unsigned int rt_prio_video1394_feed;

  pthread_t feed_thread;

  pthread_mutex_t feed_mutex;
  pthread_cond_t feed_cond;
  unsigned int feed_thread_state;

  unsigned int real_frames;     /* count of non EOF frames in play */
};

/*------------------------------------- */

static long cip_n_ntsc = 2436;
static long cip_d_ntsc = 38400;
static long cip_n_pal = 1;
static long cip_d_pal = 16;

static int fill_buffer(struct player_video1394 * player_ptr, unsigned char * targetbuf, unsigned int * packet_sizes)
{
  int i, j;
  static unsigned char continuity_counter = 0;
  static unsigned int cip_counter = 0;
  static unsigned int cip_n = 0;
  static unsigned int cip_d = 0;
  int ret;
  bool empty;
  unsigned char * packet_ptr;
  unsigned char * frame_data_ptr;

  /* get frame data to be transmission */

  ret = media_source_get_frame_data(player_ptr->media_source_ptr, (void **)&frame_data_ptr);
  if (ret < 0)
  {
    ERROR_OUT("Failed to get frame data");
    return -1;
  }

  if (ret == 0)
  {
    //ERROR_OUT("%u++", player_ptr->real_frames);
    player_ptr->real_frames++;
  }

  packet_ptr = targetbuf;

  if (cip_counter == 0)
  {
    if (!player_ptr->pal)
    {
      cip_n = cip_n_ntsc;
      cip_d = cip_d_ntsc;
    }
    else
    {
      cip_n = cip_n_pal;
      cip_d = cip_d_pal;
    }
    cip_counter = cip_n;
  }

  for (i = 0, j = 0; i < TARGETBUFSIZE && j < player_ptr->packets_count; i++)
  {
    packet_ptr = targetbuf;
    cip_counter += cip_n;

    if (cip_counter > cip_d)
    {
      empty = true;
      cip_counter -= cip_d;
    }
    else
    {
      empty = false;
    }

    *packet_ptr++ = 0x01; /* Source node ID ! */
    *packet_ptr++ = 0x78; /* Packet size in quadlets (480 / 4), is this valid for PAL too? */
    *packet_ptr++ = 0x00;
    *packet_ptr++ = continuity_counter;

    *packet_ptr++ = 0x80; /* const */
    *packet_ptr++ = player_ptr->pal ? 0x80 : 0x00;
    *packet_ptr++ = 0xff; /* timestamp */
    *packet_ptr++ = 0xff; /* timestamp */
    /* Timestamping is now done in the kernel driver! */

    if (!empty)
    {
      /* video data */
      continuity_counter++;

      memcpy(packet_ptr, frame_data_ptr + j * 480, 480);
      packet_ptr += 480;
      j++;
    }

    *packet_sizes++ = packet_ptr - targetbuf;
    targetbuf += MAX_PACKET_SIZE;
  }

  *packet_sizes++ = 0;

  media_source_next_frame(player_ptr->media_source_ptr);

  return 0;
}

#define player_ptr ((struct player_video1394 *)context)

void * player_video1394_feed(void * context)
{
  int ret;
  struct video1394_queue_variable q;
  struct video1394_wait w;
  bool playing;
  int i;

  DEBUG_OUT("player_video1394_feed thread started");

  {
    struct sched_param param;
    int policy;

    pthread_getschedparam(pthread_self(), &policy, &param);
    param.sched_priority = player_ptr->rt_prio_video1394_feed;
    policy = SCHED_FIFO;
    pthread_setschedparam(pthread_self(), policy, &param);
  }

  w.channel = q.channel = player_ptr->channel;

  /* fill kernel buffers */
  for (i = 0 ; i < player_ptr->kbuffers_count ; i++)
  {
    ret = fill_buffer(player_ptr, player_ptr->kbuffers_ptr + i * VBUF_SIZE, player_ptr->packet_sizes + i * (TARGETBUFSIZE + 1));
    if (ret < 0)
    {
      ERROR_OUT("failed to feed video1394, no data to send");
      goto exit;
    }
  }

  /* mark that we are armed for more processing */
  pthread_mutex_lock(&player_ptr->feed_mutex);

  player_ptr->feed_thread_state = FEED_THREAD_STATE_ARMED;

  ret = pthread_cond_signal(&player_ptr->feed_cond);
  if (ret != 0)
  {
    ERROR_OUT("Failed to signal feed cond (%d)", ret);
    pthread_mutex_unlock(&player_ptr->feed_mutex);
    goto exit;
  }

  pthread_mutex_unlock(&player_ptr->feed_mutex);

  playing = false;

  pthread_mutex_lock(&player_ptr->feed_mutex);

  while (player_ptr->feed_thread_state == FEED_THREAD_STATE_ARMED)
  {
    pthread_cond_wait(&player_ptr->feed_cond, &player_ptr->feed_mutex);
  }

  if (player_ptr->feed_thread_state != FEED_THREAD_STATE_PLAY)
  {
    goto exit_locked;
  }

  pthread_mutex_unlock(&player_ptr->feed_mutex);

  /* queue prefilled buffers */
  for (q.buffer = 0 ; q.buffer < player_ptr->kbuffers_count ; q.buffer++)
  {
    DEBUG_OUT("Queue...");

    q.packet_sizes = player_ptr->packet_sizes + q.buffer * (TARGETBUFSIZE + 1);

    if (ioctl(player_ptr->video1394_handle, VIDEO1394_TALK_QUEUE_BUFFER, &q) < 0)
    {
      ERROR_OUT("failed to feed video1394, VIDEO1394_TALK_QUEUE_BUFFER ioctl failed (%d).", errno);
      goto exit;
    }
  }

  playing = true;
  w.buffer = 0;

loop:
  /* wait next buffer to be sent and become unused */
  DEBUG_OUT("Wait...");
  if (ioctl(player_ptr->video1394_handle, VIDEO1394_TALK_WAIT_BUFFER, &w) < 0)
  {
    ERROR_OUT("failed to wait video1394, VIDEO1394_TALK_WAIT_BUFFER ioctl failed (%d).", errno);
    goto exit;
  }

  pthread_mutex_lock(&player_ptr->feed_mutex);

  if (player_ptr->feed_thread_state != FEED_THREAD_STATE_PLAY)
  {
    goto exit_locked;
  }

  pthread_mutex_unlock(&player_ptr->feed_mutex);

  if (player_ptr->real_frames > 0)
  {
    //ERROR_OUT("%u--", player_ptr->real_frames);
    player_ptr->real_frames--;

    if (player_ptr->real_frames == 0)
    {
      player_ptr->eod(player_ptr->eod_context);
      goto exit;
    }
  }

  /* fill the unused buffer */
  ret = fill_buffer(player_ptr, player_ptr->kbuffers_ptr + w.buffer * VBUF_SIZE, player_ptr->packet_sizes + w.buffer * (TARGETBUFSIZE + 1));
  if (ret < 0)
  {
    ERROR_OUT("failed to feed video1394, no data to send");
    goto exit;
  }

  /* queue the filled buffer */

  DEBUG_OUT("Queue...");

  q.buffer = w.buffer;
  q.packet_sizes = player_ptr->packet_sizes + q.buffer * (TARGETBUFSIZE + 1);

  if (ioctl(player_ptr->video1394_handle, VIDEO1394_TALK_QUEUE_BUFFER, &q) < 0)
  {
    ERROR_OUT("failed to feed video1394, VIDEO1394_TALK_QUEUE_BUFFER ioctl failed (%d).", errno);
    goto exit;
  }

  w.buffer++;
  w.buffer %= player_ptr->kbuffers_count;
  goto loop;

exit:
  pthread_mutex_lock(&player_ptr->feed_mutex);

exit_locked:
  player_ptr->feed_thread_state = FEED_THREAD_STATE_STOPPED;

  ret = pthread_cond_signal(&player_ptr->feed_cond);
  if (ret != 0)
  {
    ERROR_OUT("Failed to signal feed cond (%d)", ret);
  }

  pthread_mutex_unlock(&player_ptr->feed_mutex);

  DEBUG_OUT("player_video1394_feed thread stopped");
  return NULL;
}

#undef player_ptr

#define player_ptr ((struct player_video1394 *)virtual_ptr)

void
player_video1394_destroy(struct player * virtual_ptr)
{
  int channel;
  int ret;

  DEBUG_OUT("player_video1394_destroy");

  if (player_ptr->video1394_handle != -1)
  {
    /* signal thread that it must stop */
    pthread_mutex_lock(&player_ptr->feed_mutex);
    if (player_ptr->feed_thread_state != FEED_THREAD_STATE_STOPPED)
    {
      player_ptr->feed_thread_state = FEED_THREAD_STATE_STOP_REQUESTED;

      ret = pthread_cond_signal(&player_ptr->feed_cond);
      if (ret != 0)
      {
        ERROR_OUT("Failed to signal feed cond (%d)", ret);
        pthread_mutex_unlock(&player_ptr->feed_mutex);
        goto destroy_cond;
      }
    }
    pthread_mutex_unlock(&player_ptr->feed_mutex);

    ret = pthread_join(player_ptr->feed_thread, NULL);
    if (ret != 0)
    {
      ERROR_OUT("Failed to join feed thread (%d)", ret);
    }

  destroy_cond:
    ret = pthread_cond_destroy(&player_ptr->feed_cond);
    if (ret != 0)
    {
      ERROR_OUT("Failed to destroy feed cond (%d)", ret);
    }

    ret = pthread_mutex_destroy(&player_ptr->feed_mutex);
    if (ret != 0)
    {
      ERROR_OUT("Failed to destroy feed mutex (%d)", ret);
    }

    if (munmap(player_ptr->kbuffers_ptr, player_ptr->kbuffers_count * VBUF_SIZE) == -1)
    {
      ERROR_OUT("Cannot munmap video1394 device (%d)", errno);
    }

    channel = (int)player_ptr->channel;

    if (ioctl(player_ptr->video1394_handle, VIDEO1394_UNTALK_CHANNEL, &channel) < 0)
    {
      ERROR_OUT("VIDEO1394_UNTALK_CHANNEL ioctl failed (%d)", errno);
    }

    close(player_ptr->video1394_handle);
  }

  free(player_ptr);
}

int player_video1394_start_prepare(struct player * virtual_ptr)
{
  int ret;
  struct video1394_mmap v;

  DEBUG_OUT("player_video1394_start_prepare");

  NOTICE_OUT("Video: Using video1394 device \"%s\"", player_ptr->video1394_device_path);
  NOTICE_OUT("Video: Using Firewire channel %u", player_ptr->channel);
  NOTICE_OUT("Video: Using %u kernel buffers", player_ptr->kbuffers_count);
  NOTICE_OUT("Video: Using %u as SYT offset", player_ptr->syt_offset);
  NOTICE_OUT("Video: Using priority %i for feed thread", player_ptr->rt_prio_video1394_feed);

  player_ptr->video1394_handle = open(player_ptr->video1394_device_path, O_RDWR);
  if (player_ptr->video1394_handle == -1)
  {
    ERROR_OUT("Cannot open video1394 device (%d)", errno);
    ret = -1;
    goto exit;
  }

  v.channel = (int)player_ptr->channel;
  v.sync_tag = 0;
  v.nb_buffers = player_ptr->kbuffers_count;
  v.buf_size = VBUF_SIZE;
  v.packet_size = MAX_PACKET_SIZE;
  v.flags = VIDEO1394_VARIABLE_PACKET_SIZE;
  v.syt_offset = player_ptr->syt_offset;

  if (ioctl(player_ptr->video1394_handle, VIDEO1394_TALK_CHANNEL, &v) < 0)
  {
    ERROR_OUT("VIDEO1394_TALK_CHANNEL ioctl failed (%d)", errno);
    ret = -1;
    goto exit_close_device;
  }

  player_ptr->kbuffers_ptr = (unsigned char *)mmap(
         0,
         v.nb_buffers * VBUF_SIZE,
         PROT_READ|PROT_WRITE,
         MAP_SHARED,
         player_ptr->video1394_handle,
         0);
  if (player_ptr->kbuffers_ptr == (unsigned char *)-1)
  {
    ERROR_OUT("Cannot mmap video1394 device (%d)", errno);
    ret = -1;
    goto exit_untalk;
  }

  ret = pthread_mutex_init(&player_ptr->feed_mutex, NULL);
  if (ret != 0)
  {
    ERROR_OUT("Failed to init feed mutex (%d)", ret);
    ret = -1;
    goto exit_munmap;
  }

  ret = pthread_cond_init(&player_ptr->feed_cond, NULL);
  if (ret != 0)
  {
    ERROR_OUT("Failed to init feed cond (%d)", ret);
    ret = -1;
    goto exit_destroy_mutex;
  }

  player_ptr->feed_thread_state = FEED_THREAD_STATE_INITIAL;

  ret = pthread_create(&player_ptr->feed_thread, NULL, player_video1394_feed, player_ptr);
  if (ret != 0)
  {
    ERROR_OUT("Failed to start feed thread (%d)", ret);
    ret = -1;
    goto exit_destroy_cond;
  }

  /* Wait feed thread to fill kernel buffers */
  DEBUG_OUT("Waiting feed thread to fill kernel buffers...");
  pthread_mutex_lock(&player_ptr->feed_mutex);
  while (player_ptr->feed_thread_state == FEED_THREAD_STATE_INITIAL)
  {
    pthread_cond_wait(&player_ptr->feed_cond, &player_ptr->feed_mutex);
  }

  if (player_ptr->feed_thread_state != FEED_THREAD_STATE_ARMED)
  {
    ERROR_OUT("Feed thread failed to prefill kernel buffers");
    pthread_mutex_unlock(&player_ptr->feed_mutex);
    goto exit_join;
  }

  pthread_mutex_unlock(&player_ptr->feed_mutex);

  ret = 0;
  goto exit;

exit_join:
  /* signal thread that it must stop */
  pthread_mutex_lock(&player_ptr->feed_mutex);
  if (player_ptr->feed_thread_state != FEED_THREAD_STATE_STOPPED)
  {
    player_ptr->feed_thread_state = FEED_THREAD_STATE_STOP_REQUESTED;

    ret = pthread_cond_signal(&player_ptr->feed_cond);
    if (ret != 0)
    {
      ERROR_OUT("Failed to signal feed cond (%d)", ret);
      pthread_mutex_unlock(&player_ptr->feed_mutex);
      goto exit_destroy_cond;
    }
  }
  pthread_mutex_unlock(&player_ptr->feed_mutex);

  ret = pthread_join(player_ptr->feed_thread, NULL);
  if (ret != 0)
  {
    ERROR_OUT("Failed to join feed thread (%d)", ret);
  }

exit_destroy_cond:
  ret = pthread_cond_destroy(&player_ptr->feed_cond);
  if (ret != 0)
  {
    ERROR_OUT("Failed to destroy feed cond (%d)", ret);
  }

exit_destroy_mutex:
  ret = pthread_mutex_destroy(&player_ptr->feed_mutex);
  if (ret != 0)
  {
    ERROR_OUT("Failed to destroy feed mutex (%d)", ret);
  }

exit_munmap:
  if (munmap(player_ptr->kbuffers_ptr, player_ptr->kbuffers_count * VBUF_SIZE) == -1)
  {
    ERROR_OUT("Cannot munmap video1394 device (%d)", errno);
  }

exit_untalk:
  if (ioctl(player_ptr->video1394_handle, VIDEO1394_UNTALK_CHANNEL, &v.channel) < 0)
  {
    ERROR_OUT("VIDEO1394_UNTALK_CHANNEL ioctl failed (%d)", errno);
  }

exit_close_device:
  close(player_ptr->video1394_handle);
  player_ptr->video1394_handle = -1;

exit:
  return ret;
}

int player_video1394_start(struct player * virtual_ptr)
{
  int ret;

  DEBUG_OUT("player_video1394_start");

  /* signal thread that it must start play */

  pthread_mutex_lock(&player_ptr->feed_mutex);

  if (player_ptr->feed_thread_state != FEED_THREAD_STATE_ARMED)
  {
    ERROR_OUT("Feed thread is not armed");
    ret = -1;
    goto unlock;
  }

  player_ptr->feed_thread_state = FEED_THREAD_STATE_PLAY;

  ret = pthread_cond_signal(&player_ptr->feed_cond);
  if (ret != 0)
  {
    ERROR_OUT("Failed to signal feed cond (%d)", ret);
    ret = -1;
    goto unlock;
  }

  ret = 0;

unlock:
  pthread_mutex_unlock(&player_ptr->feed_mutex);

  return ret;
}

#undef player_ptr

int
player_video1394(
  struct media_source * media_source_ptr,
  eod_t eod,
  unsigned int eod_context,
  struct player ** player_ptr_ptr)
{
  int ret;
  struct player_video1394 * player_ptr;
  unsigned int format;

  DEBUG_OUT("player_video1394");

  ret = media_source_format_query(media_source_ptr, &format);
  if (ret < 0)
  {
    ret = -1;
    goto exit;
  }

  if (format != MEDIA_FORMAT_DV_PAL &&
      format != MEDIA_FORMAT_DV_NTSC)
  {
    ERROR_OUT("player_video1394 cannot handle format %u", format);
    ret = -1;
    goto exit;
  }

  player_ptr = malloc(sizeof(struct player_video1394));
  if (player_ptr == NULL)
  {
    ERROR_OUT("malloc() failed.");
    ret = -1;
    goto exit;
  }

  player_ptr->virtual.destroy = player_video1394_destroy;
  player_ptr->virtual.start_prepare = player_video1394_start_prepare;
  player_ptr->virtual.start = player_video1394_start;

  player_ptr->media_source_ptr = media_source_ptr;
  player_ptr->pal = (format == MEDIA_FORMAT_DV_PAL)?true:false;
  player_ptr->packets_count = (player_ptr->pal ? DV_FRAME_SIZE_PAL : DV_FRAME_SIZE_NTSC) / 480;

  ret = conf_file_get_string("/conf/video/device", player_ptr->video1394_device_path, sizeof(player_ptr->video1394_device_path));
  if (ret != 0)
  {
    ERROR_OUT("failed to read from configuration which video1394 device to use");
    ret = -1;
    goto free_player;
  }

  ret = conf_file_get_uint("/conf/video/channel", &player_ptr->channel);
  if (ret != 0)
  {
    ERROR_OUT("failed to read from configuration which Firewire channel to use");
    ret = -1;
    goto free_player;
  }

  ret = conf_file_get_uint("/conf/video/kbuffers_count", &player_ptr->kbuffers_count);
  if (ret != 0)
  {
    ERROR_OUT("failed to read from configuration how many video1394 kernel buffers to use");
    ret = -1;
    goto free_player;
  }

  ret = conf_file_get_uint("/conf/video/syt_offset", &player_ptr->syt_offset);
  if (ret != 0)
  {
    ERROR_OUT("failed to read from configuration what SYT offset to use for Firewire transmission");
    ret = -1;
    goto free_player;
  }

  ret = conf_file_get_uint("/conf/realtime_priorities/video1394_feed", &player_ptr->rt_prio_video1394_feed);
  if (ret != 0)
  {
    ERROR_OUT("failed to read from configuration what realtime priority to use for video1394 feed thread");
    ret = -1;
    goto free_player;
  }

  player_ptr->video1394_handle = -1;
  player_ptr->kbuffers_ptr = (unsigned char *)-1;

  player_ptr->packet_sizes = malloc(player_ptr->kbuffers_count * (TARGETBUFSIZE + 1) * sizeof(unsigned int));
  if (player_ptr->packet_sizes == NULL)
  {
    ERROR_OUT("malloc() failed.");
    ret = -1;
    goto free_player;
  }

  player_ptr->eod = eod;
  player_ptr->eod_context = eod_context;
  player_ptr->real_frames = 0;

  strcpy(player_ptr->virtual.name, "Firewire");

  *player_ptr_ptr = &player_ptr->virtual;

  ret = 0;
  goto exit;

free_player:
  free(player_ptr);

exit:
  return ret;
}
