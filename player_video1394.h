/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 *   Initialization of player capable to send DV frame sequence over FireWire
 *   This file is part of monster
 *
 *   Copyright (C) 2006,2007 Nedko Arnaudov <nedko@arnaudov.name>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; version 2 of the License
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *****************************************************************************/

#ifndef PLAYER_VIDEO1394_H__EC2AC017_ABAA_41DB_AFFF_6D7687C0D17F__INCLUDED
#define PLAYER_VIDEO1394_H__EC2AC017_ABAA_41DB_AFFF_6D7687C0D17F__INCLUDED

int
player_video1394(
  struct media_source * media_source_ptr,
  eod_t eod,
  unsigned int eod_context,
  struct player ** player_ptr_ptr);

#endif /* #ifndef PLAYER_VIDEO1394_H__EC2AC017_ABAA_41DB_AFFF_6D7687C0D17F__INCLUDED */
