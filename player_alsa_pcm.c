/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 *   Player capable to send PCM sequence over ALSA
 *   This file is part of monster
 *
 *   Copyright (C) 2006,2007 Nedko Arnaudov <nedko@arnaudov.name>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; version 2 of the License
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *****************************************************************************/

#include <alsa/asoundlib.h>
#include <pthread.h>

#define DISABLE_DEBUG_OUTPUT

#include "player.h"
#include "log.h"
#include "media_source.h"
#include "types.h"
#include "wav.h"
#include "player_alsa_pcm.h"
#include "conf.h"

#define FEED_THREAD_STATE_INITIAL         0
#define FEED_THREAD_STATE_ARMED           1
#define FEED_THREAD_STATE_PLAY            2
#define FEED_THREAD_STATE_STOP_REQUESTED  3
#define FEED_THREAD_STATE_STOPPED         4

struct player_alsa_pcm
{
  struct player virtual;

  struct media_source * media_source_ptr;
  int eof;
  eod_t eod;
  unsigned int eod_context;

  snd_pcm_t * playback_handle;

  unsigned int rt_prio_alsa_pcm_feed;

  pthread_t feed_thread;

  pthread_mutex_t feed_mutex;
  pthread_cond_t feed_cond;
  unsigned int feed_thread_state;
};

int
playback_callback(struct player_alsa_pcm * player_ptr)
{
  int ret;
  void * buffer_ptr;

  //DEBUG_OUT("playback callback called");

  if (player_ptr->eof == 0)
  {
    player_ptr->eod(player_ptr->eod_context);
    player_ptr->eof = 1;
  }

  ret = media_source_get_frame_data(player_ptr->media_source_ptr, &buffer_ptr);
  if (ret < 0)
  {
    ERROR_OUT("media_source_get_frame_data() failed.");
    return -1;
  }

  if (ret == 1 && player_ptr->eof == -1)
  {
    player_ptr->eof = 0;
  }

  ret = snd_pcm_writei(player_ptr->playback_handle, buffer_ptr, WAV_DATA_CHUNK);
  if (ret < 0)
  {
    ERROR_OUT("write failed (%s)", snd_strerror(ret));
    return -1;
  }

  if (ret != WAV_DATA_CHUNK)
  {
    ERROR_OUT("write accepted less data (%d)", ret);
    return -1;
  }

  media_source_next_frame(player_ptr->media_source_ptr);

  return 0;
}

void dump_state(const char * msg, struct player_alsa_pcm * player_ptr)
{
  snd_pcm_state_t state;

  state = snd_pcm_state(player_ptr->playback_handle);

  switch (state)
  {
  case SND_PCM_STATE_OPEN:
    DEBUG_OUT("%s: Open", msg);
    return;
  case SND_PCM_STATE_SETUP:
    DEBUG_OUT("%s: Setup installed", msg);
    return;
  case SND_PCM_STATE_PREPARED:
    DEBUG_OUT("%s: Ready to start", msg);
    return;
  case SND_PCM_STATE_RUNNING:
    DEBUG_OUT("%s: Running", msg);
    return;
  case SND_PCM_STATE_XRUN:
    DEBUG_OUT("%s: Stopped: underrun (playback) or overrun (capture) detected", msg);
    return;
  case SND_PCM_STATE_DRAINING:
    DEBUG_OUT("%s: Draining: running (playback) or stopped (capture)", msg);
    return;
  case SND_PCM_STATE_PAUSED:
    DEBUG_OUT("%s: Paused", msg);
    return;
  case SND_PCM_STATE_SUSPENDED:
    DEBUG_OUT("%s: Hardware is suspended", msg);
    return;
  case SND_PCM_STATE_DISCONNECTED:
    DEBUG_OUT("%s: Hardware is disconnected ", msg);
    return;
  }

  DEBUG_OUT("%s: Unknown ALSA pcm state %u", msg, (unsigned int)state);
}

#define player_ptr ((struct player_alsa_pcm *)context)

void * player_alsa_pcm_feed(void * context)
{
  int ret;
  snd_pcm_sframes_t frames_to_deliver;

  DEBUG_OUT("player_alsa_pcm_feed thread started");

  {
    struct sched_param param;
    int policy;

    pthread_getschedparam(pthread_self(), &policy, &param);
    param.sched_priority = player_ptr->rt_prio_alsa_pcm_feed;
    policy = SCHED_FIFO;
    pthread_setschedparam(pthread_self(), policy, &param);
  }

  /* mark that we are ready */
  pthread_mutex_lock(&player_ptr->feed_mutex);

  player_ptr->feed_thread_state = FEED_THREAD_STATE_ARMED;

  ret = pthread_cond_signal(&player_ptr->feed_cond);
  if (ret != 0)
  {
    ERROR_OUT("Failed to signal feed cond (%d)", ret);
    pthread_mutex_unlock(&player_ptr->feed_mutex);
    goto exit;
  }

  pthread_mutex_unlock(&player_ptr->feed_mutex);

  /* wait for play command */
  pthread_mutex_lock(&player_ptr->feed_mutex);

  while (player_ptr->feed_thread_state == FEED_THREAD_STATE_ARMED)
  {
    pthread_cond_wait(&player_ptr->feed_cond, &player_ptr->feed_mutex);
  }

  if (player_ptr->feed_thread_state != FEED_THREAD_STATE_PLAY)
  {
    goto exit_locked;
  }

  pthread_mutex_unlock(&player_ptr->feed_mutex);

  dump_state("Before loop", player_ptr);

loop:
  /* wait till the interface is ready for data, or 1 second has elapsed. */
  if ((ret = snd_pcm_wait(player_ptr->playback_handle, 1000)) < 0)
  {
    if (errno == 0)
    {
      /* othe thread stopped the player */
      goto exit;
    }

    ERROR_OUT("poll failed (%i: %s)", errno, strerror(errno));
    goto exit;
  }

  //DEBUG_OUT("snd_pcm_wait() returned %u", (unsigned int)ret);

  pthread_mutex_lock(&player_ptr->feed_mutex);

  if (player_ptr->feed_thread_state != FEED_THREAD_STATE_PLAY)
  {
    goto exit_locked;
  }

  pthread_mutex_unlock(&player_ptr->feed_mutex);

  /* find out how much space is available for playback data */

  do
  {
    if ((frames_to_deliver = snd_pcm_avail_update(player_ptr->playback_handle)) < 0)
    {
      if (frames_to_deliver == -EPIPE)
      {
        ERROR_OUT("an xrun occured");
        goto exit;
      }
      else
      {
        ERROR_OUT("unknown ALSA avail update return value (%d)",
                  (int)frames_to_deliver);
        goto exit;
      }
    }
  }
  while (frames_to_deliver < WAV_DATA_CHUNK);

  /* deliver the data */

  if (playback_callback(player_ptr) < 0)
  {
    ERROR_OUT("playback callback failed");
    goto exit;
  }

  if (player_ptr->eof == 1)
  {
    goto exit;
  }

  goto loop;

exit:
  pthread_mutex_lock(&player_ptr->feed_mutex);

exit_locked:
  player_ptr->feed_thread_state = FEED_THREAD_STATE_STOPPED;

  ret = pthread_cond_signal(&player_ptr->feed_cond);
  if (ret != 0)
  {
    ERROR_OUT("Failed to signal feed cond (%d)", ret);
  }

  pthread_mutex_unlock(&player_ptr->feed_mutex);

  DEBUG_OUT("player_alsa_pcm_feed thread stopped");
  return NULL;
}

#undef player_ptr

#define player_ptr ((struct player_alsa_pcm *)virtual_ptr)

void
player_alsa_pcm_destroy(struct player * virtual_ptr)
{
  int ret;

  DEBUG_OUT("player_alsa_pcm_destroy");

  if (player_ptr->playback_handle != NULL)
  {
    /* signal thread that it must stop */
    pthread_mutex_lock(&player_ptr->feed_mutex);
    if (player_ptr->feed_thread_state != FEED_THREAD_STATE_STOPPED)
    {
      player_ptr->feed_thread_state = FEED_THREAD_STATE_STOP_REQUESTED;

      ret = pthread_cond_signal(&player_ptr->feed_cond);
      if (ret != 0)
      {
        ERROR_OUT("Failed to signal feed cond (%d)", ret);
        pthread_mutex_unlock(&player_ptr->feed_mutex);
      }
      else
      {
        pthread_mutex_unlock(&player_ptr->feed_mutex);

        snd_pcm_drain(player_ptr->playback_handle);

        pthread_join(player_ptr->feed_thread, NULL);
      }
    }
    else
    {
      pthread_mutex_unlock(&player_ptr->feed_mutex);
    }

    ret = pthread_cond_destroy(&player_ptr->feed_cond);
    if (ret != 0)
    {
      ERROR_OUT("Failed to destroy feed cond (%d)", ret);
    }

    ret = pthread_mutex_destroy(&player_ptr->feed_mutex);
    if (ret != 0)
    {
      ERROR_OUT("Failed to destroy feed mutex (%d)", ret);
    }

    snd_pcm_close(player_ptr->playback_handle);
  }

  free(player_ptr);
}

int player_alsa_pcm_start_prepare(struct player * virtual_ptr)
{
  snd_pcm_hw_params_t *hw_params;
  snd_pcm_sw_params_t *sw_params;
  int ret;

  DEBUG_OUT("player_alsa_pcm_start_prepare");

  NOTICE_OUT("Audio: Using priority %i for feed thread", player_ptr->rt_prio_alsa_pcm_feed);

  if ((ret = snd_pcm_open(&player_ptr->playback_handle, "default", SND_PCM_STREAM_PLAYBACK, 0)) < 0)
  {
    ERROR_OUT("cannot open default audio device (%s)", snd_strerror(ret));
    ret = -1;
    goto exit;
  }

  if ((ret = snd_pcm_hw_params_malloc(&hw_params)) < 0)
  {
    ERROR_OUT("cannot allocate hardware parameter structure (%s)", snd_strerror(ret));
    ret = -1;
    goto exit_close_pcm;
  }

  if ((ret = snd_pcm_hw_params_any(player_ptr->playback_handle, hw_params)) < 0)
  {
    ERROR_OUT("cannot initialize hardware parameter structure (%s)", snd_strerror(ret));
    snd_pcm_hw_params_free(hw_params);
    ret = -1;
    goto exit_close_pcm;
  }

  if ((ret = snd_pcm_hw_params_set_access(player_ptr->playback_handle, hw_params, SND_PCM_ACCESS_RW_INTERLEAVED)) < 0)
  {
    ERROR_OUT("cannot set access type (%s)", snd_strerror(ret));
    snd_pcm_hw_params_free(hw_params);
    ret = -1;
    goto exit_close_pcm;
  }

  if ((ret = snd_pcm_hw_params_set_format(player_ptr->playback_handle, hw_params, SND_PCM_FORMAT_S16_LE)) < 0)
  {
    ERROR_OUT("cannot set sample format (%s)", snd_strerror(ret));
    snd_pcm_hw_params_free(hw_params);
    ret = -1;
    goto exit_close_pcm;
  }

  if ((ret = snd_pcm_hw_params_set_rate(player_ptr->playback_handle, hw_params, 48000, 0)) < 0)
  {
    ERROR_OUT("cannot set sample rate (%s)", snd_strerror(ret));
    snd_pcm_hw_params_free(hw_params);
    ret = -1;
    goto exit_close_pcm;
  }

  if ((ret = snd_pcm_hw_params_set_channels(player_ptr->playback_handle, hw_params, 2)) < 0)
  {
    ERROR_OUT("cannot set channel count (%s)", snd_strerror(ret));
    snd_pcm_hw_params_free(hw_params);
    ret = -1;
    goto exit_close_pcm;
  }

  //dump_state("Before snd_pcm_hw_params", player_ptr);

  if ((ret = snd_pcm_hw_params(player_ptr->playback_handle, hw_params)) < 0)
  {
    ERROR_OUT("cannot set parameters (%s)", snd_strerror(ret));
    snd_pcm_hw_params_free(hw_params);
    ret = -1;
    goto exit_close_pcm;
  }

  //dump_state("After snd_pcm_hw_params", player_ptr);

  snd_pcm_hw_params_free(hw_params);

  /* tell ALSA to wake us up whenever 4096 or more frames
     of playback data can be delivered. Also, tell
     ALSA that we'll start the device ourselves.
  */

  if ((ret = snd_pcm_sw_params_malloc(&sw_params)) < 0)
  {
    ERROR_OUT("cannot allocate software parameters structure (%s)", snd_strerror(ret));
    ret = -1;
    goto exit_close_pcm;
  }
  if ((ret = snd_pcm_sw_params_current(player_ptr->playback_handle, sw_params)) < 0)
  {
    ERROR_OUT("cannot initialize software parameters structure (%s)", snd_strerror(ret));
    ret = -1;
    goto exit_close_pcm;
  }
  if ((ret = snd_pcm_sw_params_set_avail_min(player_ptr->playback_handle, sw_params, 4096)) < 0)
  {
    ERROR_OUT("cannot set minimum available count (%s)", snd_strerror(ret));
    ret = -1;
    goto exit_close_pcm;
  }
  if ((ret = snd_pcm_sw_params_set_start_threshold(player_ptr->playback_handle, sw_params, 0U)) < 0)
  {
    ERROR_OUT("cannot set start mode (%s)", snd_strerror(ret));
    ret = -1;
    goto exit_close_pcm;
  }

  //dump_state("Before snd_pcm_sw_params", player_ptr);

  if ((ret = snd_pcm_sw_params(player_ptr->playback_handle, sw_params)) < 0)
  {
    ERROR_OUT("cannot set software parameters (%s)", snd_strerror(ret));
    ret = -1;
    goto exit_close_pcm;
  }

  //dump_state("Before prepare", player_ptr);

  /* the interface will interrupt the kernel every 4096 frames, and ALSA
     will wake up this program very soon after that.
  */

/*   if ((ret = snd_pcm_prepare(player_ptr->playback_handle)) < 0) */
/*   { */
/*     ERROR_OUT("cannot prepare audio interface for use (%s)", snd_strerror(ret)); */
/*     ret = -1; */
/*     goto exit_close_pcm; */
/*   } */

  ret = pthread_mutex_init(&player_ptr->feed_mutex, NULL);
  if (ret != 0)
  {
    ERROR_OUT("Failed to init feed mutex (%d)", ret);
    ret = -1;
    goto exit_close_pcm;
  }

  ret = pthread_cond_init(&player_ptr->feed_cond, NULL);
  if (ret != 0)
  {
    ERROR_OUT("Failed to init feed cond (%d)", ret);
    ret = -1;
    goto exit_destroy_mutex;
  }

  player_ptr->feed_thread_state = FEED_THREAD_STATE_INITIAL;

  ret = pthread_create(&player_ptr->feed_thread, NULL, player_alsa_pcm_feed, player_ptr);
  if (ret != 0)
  {
    ERROR_OUT("Failed to start feed thread (%d)", ret);
    ret = -1;
    goto exit_destroy_cond;
  }

  /* Wait feed thread to fill kernel buffers */
  DEBUG_OUT("Waiting feed thread to fill kernel buffers...");
  pthread_mutex_lock(&player_ptr->feed_mutex);
  while (player_ptr->feed_thread_state == FEED_THREAD_STATE_INITIAL)
  {
    pthread_cond_wait(&player_ptr->feed_cond, &player_ptr->feed_mutex);
  }

  if (player_ptr->feed_thread_state != FEED_THREAD_STATE_ARMED)
  {
    ERROR_OUT("Feed thread failed to prefill kernel buffers");
    pthread_mutex_unlock(&player_ptr->feed_mutex);
    goto exit_join;
  }

  pthread_mutex_unlock(&player_ptr->feed_mutex);

  ret = 0;
  goto exit;

exit_join:
  /* signal thread that it must stop */
  pthread_mutex_lock(&player_ptr->feed_mutex);
  if (player_ptr->feed_thread_state != FEED_THREAD_STATE_STOPPED)
  {
    player_ptr->feed_thread_state = FEED_THREAD_STATE_STOP_REQUESTED;

    ret = pthread_cond_signal(&player_ptr->feed_cond);
    if (ret != 0)
    {
      ERROR_OUT("Failed to signal feed cond (%d)", ret);
      pthread_mutex_unlock(&player_ptr->feed_mutex);
      goto exit_destroy_cond;
    }
  }
  pthread_mutex_unlock(&player_ptr->feed_mutex);

  ret = pthread_join(player_ptr->feed_thread, NULL);
  if (ret != 0)
  {
    ERROR_OUT("Failed to join feed thread (%d)", ret);
  }

exit_destroy_cond:
  ret = pthread_cond_destroy(&player_ptr->feed_cond);
  if (ret != 0)
  {
    ERROR_OUT("Failed to destroy feed cond (%d)", ret);
  }

exit_destroy_mutex:
  ret = pthread_mutex_destroy(&player_ptr->feed_mutex);
  if (ret != 0)
  {
    ERROR_OUT("Failed to destroy feed mutex (%d)", ret);
  }

exit_close_pcm:
  snd_pcm_close(player_ptr->playback_handle);
  player_ptr->playback_handle = NULL;

exit:
  return ret;
}

int player_alsa_pcm_start(struct player * virtual_ptr)
{
  int ret;

  DEBUG_OUT("player_alsa_pcm_start");

  /* signal thread that it must start play */

  pthread_mutex_lock(&player_ptr->feed_mutex);

  if (player_ptr->feed_thread_state != FEED_THREAD_STATE_ARMED)
  {
    ERROR_OUT("Feed thread is not armed");
    ret = -1;
    goto unlock;
  }

  player_ptr->feed_thread_state = FEED_THREAD_STATE_PLAY;

  ret = pthread_cond_signal(&player_ptr->feed_cond);
  if (ret != 0)
  {
    ERROR_OUT("Failed to signal feed cond (%d)", ret);
    ret = -1;
    goto unlock;
  }

  ret = 0;

unlock:
  pthread_mutex_unlock(&player_ptr->feed_mutex);

  return ret;
}

#undef player_ptr

int
player_alsa_pcm(
  struct media_source * media_source_ptr,
  eod_t eod,
  unsigned int eod_context,
  struct player ** player_ptr_ptr)
{
  int ret;
  struct player_alsa_pcm * player_ptr;
  unsigned int format;
  unsigned int rt_prio_alsa_pcm_feed;

  DEBUG_OUT("player_alsa_pcm");

  ret = media_source_format_query(media_source_ptr, &format);
  if (ret < 0)
  {
    return -1;
  }

  if (format != MEDIA_FORMAT_PCM_16_STEREO_48)
  {
    ERROR_OUT("player_alsa_pcm cannot handle format %u", format);
    return -1;
  }

  ret = conf_file_get_uint("/conf/realtime_priorities/alsa_pcm_feed", &rt_prio_alsa_pcm_feed);
  if (ret != 0)
  {
    ERROR_OUT("failed to read from configuration what realtime priority to use for ALSA PCM feed thread");
    return -1;
  }

  player_ptr = malloc(sizeof(struct player_alsa_pcm));
  if (player_ptr == NULL)
  {
    ERROR_OUT("malloc() failed.");
    return -1;
  }

  player_ptr->virtual.destroy = player_alsa_pcm_destroy;
  player_ptr->virtual.start_prepare = player_alsa_pcm_start_prepare;
  player_ptr->virtual.start = player_alsa_pcm_start;

  player_ptr->media_source_ptr = media_source_ptr;

  player_ptr->eod = eod;
  player_ptr->eod_context = eod_context;
  player_ptr->eof = -1;

  player_ptr->rt_prio_alsa_pcm_feed = rt_prio_alsa_pcm_feed;

  player_ptr->playback_handle = NULL;

  strcpy(player_ptr->virtual.name, "ALSA PCM");

  *player_ptr_ptr = &player_ptr->virtual;

  return 0;
}
