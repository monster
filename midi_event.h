/* -*- Mode: C ; c-basic-offset: 2 -*- */
/*****************************************************************************
 *
 *   MIDI event definitions
 *   This file is part of monster
 *
 *   Copyright (C) 2006,2007 Nedko Arnaudov <nedko@arnaudov.name>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; version 2 of the License
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *****************************************************************************/

#ifndef MIDI_EVENT_H__84B68D21_7453_4431_8E81_39BC4AA62F01__INCLUDED
#define MIDI_EVENT_H__84B68D21_7453_4431_8E81_39BC4AA62F01__INCLUDED

#define MIDI_EVENT_NOTE_OFF      1 /* Note Off */
#define MIDI_EVENT_NOTE_ON       2 /* Note On */
#define MIDI_EVENT_KEYPRESS      3 /* Polyphonic Key Pressure (Aftertouch) */
#define MIDI_EVENT_CC            4 /* Control Change */
#define MIDI_EVENT_PITCH         5 /* Pitch Wheel Change */
#define MIDI_EVENT_PGMCHANGE     6 /* Porgram Change */
#define MIDI_EVENT_CHANPRESS     7 /* Channel Pressure (After-touch) */
#define MIDI_EVENT_SYSEX         8 /* System Exclusive */
#define MIDI_EVENT_TEMPO         9 /* Tempo Change */
#define MIDI_EVENT_PPQN         10 /* PPQN - Pulse per quarter note */

struct midi_event
{
  unsigned int type;            /* oen of MIDI_EVENT_XXX */
  unsigned int tick;
  union
  {
    struct
    {
      unsigned int channel;
      unsigned int note;
      unsigned int velocity;
    } note_off;
    struct
    {
      unsigned int channel;
      unsigned int note;
      unsigned int velocity;
    } note_on;
    struct
    {
      unsigned int channel;
      unsigned int note;
      unsigned int velocity;
    } keypress;
    struct
    {
      unsigned int channel;
      unsigned int controller;
      unsigned int value;
    } cc;
    struct
    {
      unsigned int channel;
      unsigned int value;
    } pitch;
    struct
    {
      unsigned int channel;
      unsigned int program;
    } pgmchange;
    struct
    {
      unsigned int channel;
      unsigned int value;
    } chanpress;
    struct
    {
      unsigned int size;
      void * data;
    } sysex;
    struct
    {
      unsigned int value;
    } tempo;
    struct
    {
      unsigned int value;
    } ppqn;
  } data;
};

#endif /* #ifndef MIDI_EVENT_H__84B68D21_7453_4431_8E81_39BC4AA62F01__INCLUDED */
